const path = require('path');
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const CompressionPlugin = require('compression-webpack-plugin');

module.exports = {
   mode: 'production', // development | production
   devtool: 'inline-source-map',
   entry: {
      editUserAccountBundle: './src/userForms/editUserAccount.tsx',
      passwordResetBundle: './src/userForms/resetPassword.tsx',
      reactDashboardBundle: './src/userForms/dashboards.tsx',
      reactAdminBundle: './src/userForms/adminDash.tsx',
      reactAdminEditAccountBundle: './src/userForms/adminEditAccount.tsx',
      reactSubmitFormBundle: './src/submitForms/submitPreprint.tsx',
      reactEditFormBundle: './src/editForms/editPreprint.tsx',
      reactModerationFormBundle: './src/moderateForms/moderatePreprint.tsx',
      userAccountBundle: './src/userForms/userAccount.tsx',
      userLoginBundle: './src/userForms/login.tsx',
      userRegisterBundle: './src/userForms/userRegister.tsx',
   },
   optimization: {
      usedExports: true,
      minimize: true,
      sideEffects: true,
   },
   module: {
      rules: [
         {
            test: /\.tsx?$/,
            use: 'ts-loader',
            exclude: /node_modules/,
         },
      ],
   },
   resolve: {
      extensions: ['.tsx', '.ts', '.js'],
   },
   output: {
      filename: 'bundle/[name].js',
      path: path.resolve(__dirname, 'dist'),
   },
   plugins: [
      new CompressionPlugin(),
      // new BundleAnalyzerPlugin({
      //    analyzerMode: 'static'
      // })
   ]
};
