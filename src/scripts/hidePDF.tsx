const iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
if (iOS) {
   const embed = document.getElementById("embed");
   if (embed) {
      embed.style.display = "none";
   }
}