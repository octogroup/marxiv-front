/**
 * Setup events for local links
 */
import { GTag, DataLayer } from '../helpers/gtagInterfaces';
declare const gtag: GTag;
declare const dataLayer: DataLayer;

export const sendGAOnChangeEvent = (fieldName: string): void => gtag('event', 'field', { event_category: 'onChange', event_label: fieldName });

export const sendGADownloadEvent = (path: string): void => {
   // Path should start with /en/works/
   let workID = path.slice(10);
   if (workID.slice(workID.length - 1, workID.length) === '/') {
      workID = workID.slice(0, workID.length - 1);
   }
   return gtag('event', 'click', { event_category: 'download', event_label: workID, value: 1 })
};

export const pushToDataLayer = (obj: {[key: string]: string}) => dataLayer.push(obj);