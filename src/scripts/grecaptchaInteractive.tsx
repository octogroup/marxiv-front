import { reCAPTCHAv3SiteKey, InvokeReCAPTCHAOptions, rcptaBackoff } from './recaptchaSetup';

const options: InvokeReCAPTCHAOptions = {
   sitekey: reCAPTCHAv3SiteKey,
   action: 'interactive'
};

/**
 * We need to wait until after the reCAPTCHA script has loaded.
 * Once it has, `grecaptcha` will be defined
 */
export const runOnInteractive = () => rcptaBackoff(options);