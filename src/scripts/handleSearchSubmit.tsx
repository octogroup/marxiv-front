const searchInput: HTMLInputElement | null = document.getElementById('workSearch') as HTMLInputElement;
const searchButton: HTMLButtonElement | null = document.getElementById('workSearchButton') as HTMLButtonElement;
const searchForm = document.getElementsByTagName('form');

if (searchInput && searchButton && (searchForm && searchForm.length > 0)) {
   const form: HTMLFormElement = searchForm[0];
   
   // Determine the current language from the path, e.g. `/en/`
   const langPrefix = window.location.pathname.slice(0, 4);

   searchButton.addEventListener('click', () => {
      // Get the contents of the search input
      let query = searchInput.value;
      
      // Redirect the user to the search page with their query appended
      window.location.href = window.location.origin + langPrefix + "search?q=" + query;
   });

   form.addEventListener('submit', (ev: Event) => {
      // Prevent default behavior of reloading the current page with a query parameter.
      ev.preventDefault();

      // Get the contents of the search input
      let query = searchInput.value;

      // Redirect the user to the search page with their query appended
      window.location.href = window.location.origin + langPrefix + "search?q=" + query;
   });
}