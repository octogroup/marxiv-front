
/**
 * Setup Collapsible Blocks for the Submission Guidelines
 */
const addListenerByID = (id: string) => {
   let min = document.getElementById(id + 'Min');
   let minBar = document.getElementById(id + 'MinCollapseBar');
   let max = document.getElementById(id + 'Max');
   let maxBar = document.getElementById(id + 'MaxCollapseBar');

   // Max state
   if (max && maxBar) {
      // Add event listener
      maxBar.addEventListener('click', () => {
         min ? min.hidden = false : null
         max ? max.hidden = true : null
      });
      // Initially hide the Max state
      max.hidden = true;
   }

   // Min state
   if (minBar) {
      // Add event listener
      minBar.addEventListener('click', () => {
         max ? max.hidden = false : null
         min ? min.hidden = true : null
      })
   }
};

const submissionGuidelinesCollapsibles: string[] = [
   'beforeSubmit',
   'transferredCopyright',
   'postGuidelines',
   'licenseGuidelines',
   'preprintGuidelines',
   'ctaGuidelines',
   'publishGuidelines',
];

for (let i = 0; i < submissionGuidelinesCollapsibles.length; i++) {
   addListenerByID(submissionGuidelinesCollapsibles[i])
};