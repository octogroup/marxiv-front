let id = 'UA-';

window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', id, {
   'link_attribution': {
      levels: 5
   },
   'allow_ad_personalization_signals': false
});
