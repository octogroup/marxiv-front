/**
 * Invoke reCAPTCHA v3 on page-load and user-interaction
 */
import { reCAPTCHAv3SiteKey, InvokeReCAPTCHAOptions, rcptaBackoff } from './recaptchaSetup';

const options: InvokeReCAPTCHAOptions = {
   sitekey: reCAPTCHAv3SiteKey,
   action: 'page'
};

/**
 * We need to wait until after the reCAPTCHA script has loaded.
 * Once it has, `grecaptcha` will be defined
 */
rcptaBackoff(options);