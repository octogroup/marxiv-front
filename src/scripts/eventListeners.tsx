import { captureOutboundLink } from './captureOutboundLink';
import { captureLocalLink } from './captureLocalLink';
import { sendGAOnChangeEvent, sendGADownloadEvent } from './sendGTagEvent';

import { runOnInteractive } from './grecaptchaInteractive';

/**
 * Helper functions for onClickSetup.tsx
 */
export const isLocalLink = (link: string): boolean => {
   const prefixes = {
      http: 'http://',
      https: 'https://'
   };
   const domain = {
      test: 'marxiv.test',
      live: 'marxiv.org'
   };
   const testing = prefixes.http + domain.test;
   if (link.startsWith(testing)) {
      return true
   }
   else {
      return false
   }
}

export const addLinkEventListener = (): void => {
   const links: HTMLCollectionOf<HTMLAnchorElement> = document.getElementsByTagName('a');
   if (links) {
      for (let i = 0; i < links.length; i++) {
         let link: HTMLAnchorElement = links[i];
         let destination = link.href;
         if (isLocalLink(destination)) {
            // This is an internal link, so just add the reCAPTCHA CBs
            link.addEventListener('click', () => {
               console.log('Before runOnInteractive LocalLink click');
               runOnInteractive()
               return captureLocalLink(destination);
            });
            link.addEventListener('auxclick', () => {
               console.log('Before runOnInteractive LocalLink auxclick');
               runOnInteractive();
               return captureLocalLink(destination);
            });
            link.addEventListener('contextmenu', () => {
               console.log('Before runOnInteractive LocalLink contextmenu');
               runOnInteractive();
               return captureLocalLink(destination);
            });
         }
         else {
            // Add GAnalytics and reCAPTCHA CBs for outbound links
            link.addEventListener('click', () => {
               console.log('Before runOnInteractive OutboundLink click');
               runOnInteractive();
               return captureOutboundLink(destination);
            });
            link.addEventListener('auxclick', () => {
               console.log('Before runOnInteractive OutboundLink auxclick');
               runOnInteractive();
               return captureOutboundLink(destination);
            });
            link.addEventListener('contextmenu', () => {
               console.log('Before runOnInteractive OutboundLink contextmenu');
               runOnInteractive();
               return captureOutboundLink(destination);
            });
         }
      }
   }
   else {
      return
   }
}

export const addButtonEventListener = (): void => {
   const buttons: HTMLCollectionOf<HTMLButtonElement> = document.getElementsByTagName('button');
   if (buttons) {
      for (let i = 0; i < buttons.length; i++) {
         let button: HTMLButtonElement = buttons[i];
         button.addEventListener('click', () => {
            console.log('Before runOnInteractive Button click');
            runOnInteractive()
         });
         button.addEventListener('auxclick', () => {
            console.log('Before runOnInteractive Button auxclick');
            runOnInteractive()
         });
         button.addEventListener('contextmenu', () => {
            console.log('Before runOnInteractive Button contextmenu');
            runOnInteractive()
         });
      }
   }
   else {
      return
   }
}

export const addFieldEventListener = (): void => {
   const inputs: HTMLCollectionOf<HTMLInputElement> = document.getElementsByTagName('input');
   if (inputs) {
      for (let i = 0; i < inputs.length; i++) {
         let field: HTMLInputElement = inputs[i];
         field.addEventListener('blur', () => {
            console.log('Before runOnInteractive Field blur');
            runOnInteractive();
            sendGAOnChangeEvent(field.name);
         });
      }
   }
}

export const addDownloadEventListener = (): void => {
   const downloadButton = document.querySelector('a.download');
   const path = document.location.pathname;
   if (downloadButton) {
      downloadButton.addEventListener('click', () => {
         console.log('Before runOnInteractive Download click');
         runOnInteractive();
         return sendGADownloadEvent(path);
      });
      downloadButton.addEventListener('auxclick', () => {
         console.log('Before runOnInteractive Download auxclick');
         runOnInteractive();
         return sendGADownloadEvent(path);
      });
      downloadButton.addEventListener('contextmenu', () => {
         console.log('Before runOnInteractive Download contextmenu');
         runOnInteractive();
         return sendGADownloadEvent(path);
      });
   }
}

export function addEventListeners () {
   addLinkEventListener();
   addButtonEventListener();
   addFieldEventListener();
   addDownloadEventListener();
}