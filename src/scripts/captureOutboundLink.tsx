/**
 * Capture outbound-link clicks for Google Analytics
 */
import { GTag } from '../helpers/gtagInterfaces';
declare const gtag: GTag;

/**
 * Outbound link clicks need to return `false` to prevent the browser's default behavior, i.e. taking you to the offsite page immediately.
 * Doing so would stop JS from executing, so we need to tell GA about the click *before* sending the user to the link's destination.
 */
export const captureOutboundLink = (link: string): false => {
   gtag('event', 'click',{
      event_category: 'outboundLink',
      event_label: link,
      transport_type: 'beacon',
      event_callback: () => {document.location.href = link}
   })
   return false
}