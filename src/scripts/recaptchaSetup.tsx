export const reCAPTCHAv3SiteKey = '';

/**
 * You get a "detailed break-down" of your top 10 actions in the admin console
 * We'll use 8 static actions, so as not to exceed the soft-limit of 10.
 */
export interface reCAPTCHAHomepage {
   action: 'homepage'
}
export interface reCAPTCHAPage {
   action: 'page'
}
export interface reCAPTCHARegister {
   action: 'register'
}
export interface reCAPTCHALogin {
   action: 'login'
}
export interface reCAPTCHAResetPassword {
   action: 'reset'
}
/**
 * General "low-key" forms, like surveys (and user account & edit pages)
 */
export interface reCAPTCHAForm {
   action: 'form'
}
/**
 * More important forms, like the MarXiv submission form (and edit, moderate)
 */
export interface reCAPTCHAFormSubmit {
   action: 'submit'
}
/**
 * Any kind of interactive component, e.g. link, button, etc.
 */
export interface reCAPTCHAInteractive {
   action: 'interactive'
}
/**
 * Union type for our static actions.
 * @param action May only contain alphanumeric characters and slashes, and must not be user-specific.
 */
export type reCAPTCHAAction = reCAPTCHAHomepage | reCAPTCHAPage | reCAPTCHARegister | reCAPTCHALogin | reCAPTCHAResetPassword | reCAPTCHAForm | reCAPTCHAInteractive | reCAPTCHAFormSubmit;
export type reCAPTCHAActionStrings = 'page' | 'homepage' | 'register' | 'login' | 'reset' | 'form' | 'submit' | 'interactive';

/**
 * Callback functions to operate onSubmit, onError, and onRetry
 * 
 * @param onInteraction Callback to run onClick or onSubmit or whatever, with verification if needed.
 * @param onError Only use in conjunction with verification.
 */
export interface reCAPTCHACallbacks {
   onInteraction: (token: string) => void
   onError: () => void
}

/**
 * Let's not work with promises directly!
 * @param sitekey reCAPTCHA v3 site key. Register one at https://g.co/recaptcha/v3
 * @param action 1 of 8 standardized reporting actions, which are simple strings.
 * @param reCAPTCHACallbacks If the user's interaction needs to be verified, include this in the onInteraction callback, which must handle humans and bots.
 */
export type InvokeReCAPTCHAOptions = reCAPTCHAAction & {
   sitekey: string
   reCAPTCHAcbs?: reCAPTCHACallbacks
}

/**
 * @param reCAPTCHAv3Key reCAPTCHA v3 site key. Register one at https://g.co/recaptcha/v3
 */
export interface reCAPTCHA {
   ready(callback: () => void): void
   execute(reCAPTCHAv3Key: string, action: reCAPTCHAAction): Promise<string>
}
// Establish grecaptcha.
declare const grecaptcha: reCAPTCHA;

export const reCAPTCHExecute = (options: InvokeReCAPTCHAOptions): void => {
   if (options.reCAPTCHAcbs) {
      // This reCAPTCHA requires verification.
      return grecaptcha.ready(
         () => {
            // TypeScript compiler forces us to re-check.
            if (options.reCAPTCHAcbs) {
               console.log("Executing reCAPTCHExecute with cbs with action: " + options.action);
               grecaptcha.execute(options.sitekey, { action: options.action }).then(options.reCAPTCHAcbs.onInteraction, options.reCAPTCHAcbs.onError)
            }
            else {
               throw new Error('reCAPTCHA Callbacks are undefined in an impossible situation.')
            }
         }
      )
   }
   else {
      return grecaptcha.ready(
         () => {
            console.log("Executing reCAPTCHExecute with action: " + options.action);
            grecaptcha.execute(options.sitekey, { action: options.action });
         }
      )
   }
}

/**
 * reCAPTCHA helpers
 */
export const rcptaBackoff = (options: InvokeReCAPTCHAOptions) => window.setTimeout(() => {
   console.log("Executing rcptaBackoff with action: " + options.action);
   if (grecaptcha) {
      reCAPTCHExecute(options);
   }
   else {
      window.setTimeout(() => {
         if (grecaptcha) {
            reCAPTCHExecute(options);
         }
         else {
            window.setTimeout(() => {
               if (grecaptcha) {
                  reCAPTCHExecute(options);
               }
               else {
                  console.log('Waited 10s but could not execute grecaptcha.')
               }
            }, 5000)
         }
      }, 3000)
   }
}, 2000);