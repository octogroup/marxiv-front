/**
 * Invoke reCAPTCHA v3 on page-load and user-interaction
 */
import { runOnInteractive } from './grecaptchaInteractive';

console.log("onLoadInteractive is going to call runOnInteractive().")
runOnInteractive();