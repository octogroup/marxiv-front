/**
 * Show/Hide the MarXiv Logo and the "User Menu" when the mobile main menu is toggled
 */
// We need to explicitly state that #mainMenuInput is an HTMLInputElement so we can call the 'checked' prop.
const mainMenuCheckbox: HTMLInputElement = document.getElementById('mainMenuInput') as HTMLInputElement;

const marxivLogo = document.getElementById('marxivLogoWrapper');

const shareUserMenu = document.getElementById('shareMenuWrapper');

// Set an event listener for clicks on the Main Menu Checkbox
if (mainMenuCheckbox) {
   mainMenuCheckbox.addEventListener('change', () => {
      if (mainMenuCheckbox.checked) {
         // Hide the logo and share menu
         if (marxivLogo && shareUserMenu) {
            marxivLogo.style.display = 'none';
            shareUserMenu.style.display = 'none';
         }
      }
      else {
         // Show the logo and share menu
         if (marxivLogo && shareUserMenu) {
            marxivLogo.style.display = 'block';
            shareUserMenu.style.display = 'block';
         }
      }
   })
}