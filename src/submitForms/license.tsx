import * as React from 'react';
import { Translatables, SelectOption, makeLicenseOptions, LicenseOption } from '@octogroup/marxiv-common';
import Select from 'react-select';

import { validateLicense, validateNotAPublisher } from '../helpers/validations';
import { FormErrorMessage, hastToReact, getDefaultValue, } from '../helpers/formHelpers';

/**
 * License and Copyright Holder
 */
export interface LicenseState {
   license: LicenseOption | ''
   copyrightHolder: string
}

/**
 * Conditionally render the copyright holder field
 */
const CopyrightHolder = React.memo(({ register, unregister, errors, setValue, d }: { register: any, unregister: any, errors: Partial<Record<string, any>>, setValue: any, d: Translatables}) => {
   React.useEffect(() => {
      register({ name: 'copyrightHolder' }, { validate: validateNotAPublisher(d.forms.messages.errors.copyright) });
      return () => {
         unregister({ name: 'copyrightHolder' });
      };
   }, [register, unregister]);

   return <div className='copyright'>
      <div className='field'>
         <div className='description verticalBuffer2'>
            {d.forms.submit.license.noLicense}
         </div>
      </div>
      <div className='field input shortText flex all-parent-row center'>
         <label htmlFor='copyrightHolder'>
            {d.forms.submit.license.copyrightHolder.fieldTitle}
         </label>
         <input
            placeholder={d.forms.submit.license.copyrightHolder.default}
            name='copyrightHolder'
            id='copyrightHolder'
            type='text'
            className={errors.copyrightHolder && errors.copyrightHolder.message ? 'invalid' : 'required'}
            ref={register({
               validate: validateNotAPublisher(d.forms.messages.errors.copyright)
            })}
            onBlur={(event) => setValue('copyrightHolder', event.currentTarget.value)}
         />
      </div>
      {errors.copyrightHolder && errors.copyrightHolder.message ? (<FormErrorMessage>{errors.copyrightHolder.message}</FormErrorMessage>) : null}
   </div>
});

/**
 * Make the License and Copyright-holder form
 */
export const HookLicenseCopyright = function ({ d, initialValues, isPostprint, register, unregister, watch, errors, setValue }: { d: Translatables, initialValues: LicenseState, isPostprint: boolean, register: any, unregister: any, watch: any, errors: any, setValue: any }) {
   const selectedLicense = watch('license');

   // Register values for React-state controlled data
   React.useEffect(() => {
      register({ name: 'license' }, { validate: validateLicense(d.forms) });
   }, [register]);

   // Set our Select options
   const licenseSelectOptions: SelectOption[] = makeLicenseOptions(d);
   const initialLicenseSelectionOption: SelectOption = getDefaultValue(initialValues.license)(licenseSelectOptions);

   // Set our initial values for React-state controlled data
   const [values, setReactSelectValue] = React.useState({
      selectedLicenseOption: initialLicenseSelectionOption
   });

   const handleLicenseChange = (selectedLicenseOption: SelectOption): void => {
      setValue('license', selectedLicenseOption.value);
      setReactSelectValue({ selectedLicenseOption: selectedLicenseOption })
   };

   /**
    * Render the content
    */
   return <div id='workLicense'>
      <div className='form wrapper'>
         <div className='title flex all-child-span1'>
            {d.forms.submit.license.formTitle}
         </div>
         <div className='content'>

            <div className='field'>
               {isPostprint ? (<div className='description' dangerouslySetInnerHTML={hastToReact(d.forms.submit.license.descriptionPostprint)} />) : (<div className='description' dangerouslySetInnerHTML={hastToReact(d.forms.submit.license.description)} />) }
            </div>

            <div className='field selectList flex all-parent-row center'>
               <label htmlFor='license'>
                  {d.forms.submit.license.license.formTitle}
               </label>
               <Select
                  options={licenseSelectOptions}
                  isMulti={false}
                  isSearchable={false}
                  name="license"
                  id='license'
                  ref={register}
                  value={values.selectedLicenseOption}
                  onChange={handleLicenseChange}
               />
            </div>
            {errors.license ? (<FormErrorMessage>{d.forms.messages.errors.license}</FormErrorMessage>) : null}

            {selectedLicense === 'noLicense' && (
               <CopyrightHolder {...{ register, unregister, errors, setValue, d }} />
            )}
            
         </div>
      </div>
   </div>
}
