import * as React from 'react';
import { Translatables, } from '@octogroup/marxiv-common';

import { FormErrorMessage, } from '../helpers/formHelpers';
import { required } from '../helpers/validations';

/**
 * PDF File Upload
 */
export interface FileUploadState {
   file: ArrayBuffer | File | string
   fileData: ArrayBuffer | File | string
}

export const HookFileUpload = function ({ d, isEditForm, register, errors, setValue, watch }: { d: Translatables, isEditForm: boolean, register: any, errors: any, setValue: any, watch: any }) {
   // Register values for React-state controlled data
   React.useEffect(() => {
      register({ name: 'fileData' }, { validate: required(d.forms.messages.errors.fileMissing) });
   }, [register]);

   // Watch for fileData to hide the drop-zone and file-input
   const fileLoaded = watch('fileData');

   // Supply a description for edit forms.
   const editDescription = <div className='field'>
      <div className='description'>
         {d.forms.submit.files.editDescription}
      </div>
   </div>

   /**
    * Handle file uploads to the backend.
    */
   function uploadToOcto(fileData: string): void {
      // Errors
      const invalid = d.forms.messages.errors.fileInvalid;
      const uploadError = d.forms.messages.errors.fileUploadError;
      // Success
      const success = d.forms.messages.confirmations.fileSaved;
   }

   /**
    * Drag and Drop
    */
   function handleDragOver(event: React.DragEvent): void {
      event.preventDefault();
      event.stopPropagation();

      const dz = document.getElementById('dropZone');

      if (dz) {
         dz.style.boxShadow = "0 0 0.5rem #0C98B8"
      }
   };

   function handleDragLeave(event: React.DragEvent): void {
      event.preventDefault();
      event.stopPropagation();

      const dz = document.getElementById('dropZone');

      if (dz) {
         dz.style.boxShadow = "none"
      }
   }

   function handleDrop(event: React.DragEvent): void {
      event.preventDefault();
      event.stopPropagation();

      console.log('Handle drop triggered.')

      const dz = document.getElementById('dropZone');

      if (dz) {
         dz.style.boxShadow = "none"
      }

      if (event.dataTransfer.items && (event.dataTransfer.items.length > 0)) {
         const droppedItem: DataTransferItem = event.dataTransfer.items[0];
         console.log("Item dropped.");

         if (droppedItem.kind === 'file') {
            console.log('Dropped item is a file.')
            const droppedFile = droppedItem.getAsFile();
            if (droppedFile) {

               // Ensure the file is a PDF
               if (droppedFile.type === 'application/pdf') {
                  // Launch the File Reader API (https://developer.mozilla.org/en-US/docs/Web/API/FileReader)
                  const reader = new FileReader();
                  // Once the Reader launches, we can extract some data from the file.
                  // The onload is possibly null, so we can't invoke it directly.
                  reader.onload = (function (ev: ProgressEvent<FileReader>) {
                     // We need to drill-down to the processed file
                     const evTarget = ev.target;

                     if (evTarget && evTarget.result) {
                        const fileData = evTarget.result;

                        console.log('event.target.result:');
                        console.log(fileData);

                        // Save the file data into the form state.
                        setValue('fileData', fileData);
                     }
                  });
                  // Actually read the file data, then save the data in the parent form state.
                  // or, reader.readAsBinaryString(droppedFile)
                  reader.readAsDataURL(droppedFile);
               }
               else {
                  // The file is not a PDF
                  alert(d.forms.messages.errors.fileTypePDF)
               }

            }
         }
         else {
            console.log('*** Dropped item is NOT a file ***');
         }
      }
   };

   function handleFileInput(): void {
      const fileInput: HTMLInputElement = document.getElementById('file') as HTMLInputElement;

      if (fileInput && fileInput.files && (fileInput.files.length > 0)) {
         const fileVal = fileInput.files[0];

         console.log('fileVal:')
         console.log(fileVal);

         // Launch the File Reader API (https://developer.mozilla.org/en-US/docs/Web/API/FileReader)
         const reader = new FileReader();
         // Once the Reader launches, we can extract some data from the file.
         // The onload is possibly null, so we can't invoke it directly.
         reader.onload = (function (ev: ProgressEvent<FileReader>) {
            // We need to drill-down to the processed file
            const evTarget = ev.target;

            if (evTarget && evTarget.result) {
               const fileData = evTarget.result;

               console.log('event.target.result:');
               console.log(fileData);

               // Save the file data into the form state.
               setValue('fileData', fileData);
            }
         });
         // Actually read the file data, then save the data in the parent form state.
         reader.readAsDataURL(fileVal);
      }
   }

   /**
    * Reset the form
    */
   const resetFileForm = (): void => {
      setValue('file', '');
      setValue('fileData', '');
   }

   /**
    * Render the content
    */
   return <div id='fileUpload'>
      <div className='form wrapper'>
         <div className='title flex all-child-span1'>
            {d.forms.submit.files.pdf.formTitle}
         </div>
         {fileLoaded ? (<div className='content' id='uploadStatus'>
            {d.forms.submit.files.uploadingFile}
            <button type='button' className='button cancel'>
               {d.forms.submit.files.cancelUpload}
            </button>
            <button type='button' className='button orange' onClick={resetFileForm}>
               {d.forms.submit.files.removeFile}
            </button>
         </div>) : (<div className='content' id='dropZone' onDrop={handleDrop} onDragOver={handleDragOver} onDragLeave={handleDragLeave} >

            {isEditForm ? (editDescription) : null}

            <div className='center'>
               {d.forms.submit.files.pdf.dragAndDrop}
            </div>

            <div className='separator'>
               <div>
                  {d.forms.submit.files.pdf.or}
               </div>
            </div>

            <div className='flex all-parent-column'>
               <div className='flex all-child-span1 center'>
                  <input
                     ref={register({
                        validate: required(d.forms.messages.errors.fileMissing)
                     })}
                     name='file'
                     id='file'
                     type='file'
                     multiple={false}
                     accept='.pdf,application/pdf'
                     onChange={() => handleFileInput()}
                  />
               </div>
            </div>

            {errors.file ? (<div className='verticalBuffer1'><FormErrorMessage>{d.forms.messages.errors.fileMissing}</FormErrorMessage></div>) : null}

         </div>)}

      </div>
   </div>
}

// const handleFileUpload = (event: React.ChangeEvent<HTMLInputElement>): void => {
//    if (event.target.files && event.target.files.length > 0) {
//       console.log('event.target.files[0]:');
//       localLog(null)(event.target.files[0]);
//       // Launch the File Reader API
//       const reader = new FileReader();
//       // Once the Reader launches, we can extract some data from the file.
//       // The onload is possibly null, so we can't invoke it directly.
//       reader.onload = (function (ev: ProgressEvent<FileReader>) {
//          // We can access the progess of the file upload/reading process
//          if (ev.lengthComputable) {
//             let max = ev.total;
//             let done = ev.loaded;
//             if (done === max) {
//                // The file is read
//                console.log('The file has been read.');
//             }
//             else {
//                // The file is still being read
//                let diff = done / max;
//                let diffMod = done % max;
//                console.log(diff);
//                console.log(diffMod);
//             }
//          }
//          // We need to drill-down to the processed file
//          const evTarget = ev.target;
//          if (evTarget) {
//             const fileData = evTarget.result;
//             console.log('event.target.result:');
//             localLog(null)(fileData);
//             // Save the file data into the form state.
//             return setValue('file', fileData);
//          }
//       });
//       // Actually read the file data.
//       // Note that readAsArrayBufffer does not work from testing for PDFs.
//       // readAsBinaryString seems to try and parse the PDF.
//       // readAsDataURL yields `'data:application/pdf;base64,JVBERi0xLjQKJdPr6...'`
//       reader.readAsDataURL(event.target.files[0]);
//       // reader.readAsBinaryString(event.target.files[0]);
//    }
// };