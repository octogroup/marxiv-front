import * as React from 'react';
import { render } from "react-dom";
import { Translatables, translatablesEnglish, LicenseOption, englishMarXivMinimalSitemap } from '@octogroup/marxiv-common';
import { MarXivSitemapMinimum } from '@octogroup/marxiv-common/dist/sitemap';
import useForm from 'react-hook-form';
import gql from 'graphql-tag';

import {Contributor, hastToReact, Relationship, determineSubmitFormType, FormType } from '../helpers/formHelpers';
import { addEventListeners } from '../scripts/eventListeners';
import { reCAPTCHAv3SiteKey, InvokeReCAPTCHAOptions, rcptaBackoff, } from '../scripts/recaptchaSetup';

import { HookContribs } from './contributors';
import { HookFileUpload, FileUploadState } from './upload';
import { HookBasicInfo, PostedContentState } from './basicInfo';
import { HookRelationships } from './relationships';
import { HookLicenseCopyright, LicenseState } from './license';
import { HookDonation } from './donation';

export interface SubmissionFormState extends FileUploadState, PostedContentState, LicenseState {
   // Contribs
   contributors: Contributor[]
   // Relationships between works
   relationships: Relationship[]
   // // Publisher
   // publisher: Publisher
   // // Conference Paper
   // conferencePaper: ConferencePaper
   // // Database
   // database: Database
   // // Dataset
   // parentDatabaseDOI: string
   // // Peer Review
   // peerReview: PeerReview
   // // Institutions
   // institutions: Institution[]
   // // Citations
   // citations: Citation[]
};

export interface SubmissionData {
   fileData: ArrayBuffer | File | string
   title: string
   language: string
   peerReviewed: boolean
   subtitle: string | undefined
   publisherDOI: string | undefined
   description: string
   originalPublicationYear: string
   originalPublicationMonth: string | undefined
   originalPublicationDay: string | undefined
   license: LicenseOption
   copyrightHolder: string | undefined
   contributors: Contributor[]
   relationships: Relationship[] | undefined
}

const initialFormState: SubmissionFormState = {
   // File upload
   file: '',
   fileData: '',
   // Basic information
   title: '',
   language: 'en',
   peerReviewed: 'notPeerReviewed',
   subtitle: '',
   publisherDOI: '',
   // Original publication date
   originalPublicationYear: '',
   originalPublicationMonth: '',
   originalPublicationDay: '',
   // License & copyright
   license: '',
   copyrightHolder: '',
   // Description
   description: '',
   // Contribututors
   contributors: [],
   // Relationships between works
   relationships: [],
};

export const formToData = (form: SubmissionFormState): SubmissionData => ({
   fileData: form.fileData,
   title: form.title,
   language: form.language,
   peerReviewed: form.peerReviewed === 'peerReviewed' ? true : false,
   subtitle: form.subtitle && form.subtitle.length > 0 ? form.subtitle : undefined,
   publisherDOI: form.publisherDOI && form.publisherDOI.length > 0 ? form.publisherDOI : undefined,
   description: form.description,
   originalPublicationYear: form.originalPublicationYear,
   originalPublicationMonth: form.originalPublicationMonth && form.originalPublicationMonth.length > 0 ? form.originalPublicationMonth : undefined,
   originalPublicationDay: form.originalPublicationDay && form.originalPublicationDay.length > 0 ? form.originalPublicationDay : undefined,
   license: form.license as LicenseOption,
   copyrightHolder: form.copyrightHolder && form.copyrightHolder.length > 0 ? form.copyrightHolder : undefined,
   contributors: form.contributors,
   relationships: form.relationships && form.relationships.length > 0 ? form.relationships : undefined
});

const graphqlSubmission = gql`
   mutation SubmitPreprintForm($formData: SubmissionForm!) {
      submitPreprint(form: $formData) {
         form
      }
   }`;

const HookSubmitPreprint = function ({ d, siteMap }: { d: Translatables, siteMap: MarXivSitemapMinimum }) {
   // const [submitMut] = useMutation<SubmissionData, SubmissionData>(graphqlSubmission);
   
   const { handleSubmit, register, unregister, errors, watch, setValue, getValues } = useForm<SubmissionFormState>({
      mode: 'onBlur',
      reValidateMode: 'onBlur',
      defaultValues: initialFormState
   });

   React.useEffect(() => {
      // Add event listeners just on load
      return addEventListeners();
   }, []);

   /**
    * Redirect the user to the confirmation page.
    */
   const confirmationRedirect = (): string => {
      const path: string = window.location.pathname; // "/en/submit/preprint/"
      const lang: string = path.slice(0, 3); // "/en"
      const confirmationPath: string = lang + siteMap.confirm.newSubmission.localURL;
      return window.location.pathname = confirmationPath;
   }

   const onSubmitCB = (data: SubmissionFormState) => {
      console.log("in onSubmitCB.");
      const convertedData: SubmissionData = formToData(data);
      console.log("Submitted parent form with converted data:");
      console.log(convertedData);
      // submitMut({ variables: convertedData });

      // Redirect the user to the confirmation page
      // confirmationRedirect();
   };

   /**
    * Determine the form type
    */
   const formType: FormType = determineSubmitFormType();

   const isPostprint: boolean = formType === 'postprint' ? true : false;

   const getFormTitle = (formType: FormType): string => {
      switch (formType) {
         case 'preprint':
            return d.forms.submit.titles.preprint;
         case 'postprint':
            return d.forms.submit.titles.postprint;
         case 'oa':
            return d.forms.submit.titles.openAccess;
         case 'uncopyrighted':
            return d.forms.submit.titles.unCopyrighted;
         case 'reportUnpublished':
            return d.forms.submit.titles.unpublishedReport;
         case 'thesis':
            return d.forms.submit.titles.thesis;
         case 'poster':
            return d.forms.submit.titles.posterPresentationSI;
         case 'letter':
            return d.forms.submit.titles.letter;
         case 'workingPaper':
            return d.forms.submit.titles.workingPaper;
         default:
            return d.forms.submit.titles.preprint;
      }
   };

   /**
    * We need to wait until after the reCAPTCHA script has loaded.
    * Once it has, `grecaptcha` will be defined
    */
   const onLoadSubmit: InvokeReCAPTCHAOptions = {
      sitekey: reCAPTCHAv3SiteKey,
      action: 'submit'
   };

   return <div id='uploadPreprint'>
      <h1>
         {getFormTitle(formType)}
      </h1>
      <div className='description' dangerouslySetInnerHTML={hastToReact(d.forms.submit.topDisclaimer)} />
      <div className='verticalBuffer1'>
         
         <form id='form' className='form'>

            <div className='verticalBuffer1'>
               <HookFileUpload
                  d={translatablesEnglish.translatables}
                  isEditForm={false}
                  register={register}
                  setValue={setValue}
                  errors={errors}
                  watch={watch}
               />
            </div>

            <div className='verticalBuffer1'>
               <HookBasicInfo
                  d={translatablesEnglish.translatables}
                  initialValues={{
                     title: initialFormState.title,
                     language: initialFormState.language,
                     peerReviewed: initialFormState.peerReviewed,
                     subtitle: initialFormState.subtitle,
                     publisherDOI: initialFormState.publisherDOI,
                     description: initialFormState.description,
                     originalPublicationYear: initialFormState.originalPublicationYear,
                     originalPublicationMonth: initialFormState.originalPublicationMonth,
                     originalPublicationDay: initialFormState.originalPublicationDay,
                  }}
                  isPostprint={isPostprint}
                  register={register}
                  setValue={setValue}
                  errors={errors}
               />
            </div>

            <div className='verticalBuffer1'>
               <HookContribs
                  d={translatablesEnglish.translatables}
                  register={register}
                  setValue={setValue}
                  getValues={getValues}
               />
            </div>

            <div className='verticalBuffer1'>
               <HookRelationships
                  d={translatablesEnglish.translatables}
                  register={register}
                  setValue={setValue}
                  getValues={getValues}
               />
            </div>

            <div className='verticalBuffer1'>
               <HookLicenseCopyright
                  d={translatablesEnglish.translatables}
                  initialValues={{
                     license: initialFormState.license,
                     copyrightHolder: initialFormState.copyrightHolder,
                  }}
                  isPostprint={isPostprint}
                  register={register}
                  unregister={unregister}
                  watch={watch}
                  errors={errors}
                  setValue={setValue}
               />
            </div>

            <div className='verticalBuffer1'>
               <HookDonation
                  d={translatablesEnglish.translatables}
               />
            </div>

            <div className='form end flex all-parent-row'>
               <div className='flex all-child-span1 center'>
                  <button type='reset' className='button cancel'>
                     {d.commonReplacements.cancel}
                  </button>
               </div>
               <div className='flex all-child-span1 center'>
                  <button type='button' className='button submit' onClick={() => {
                     console.log('On button click');
                     const vals = getValues({ nest: true });
                     onSubmitCB(vals);
                  }}>
                     {d.commonReplacements.submit}
                  </button>
               </div>
            </div>

         </form>
         
      </div>

      {() => rcptaBackoff(onLoadSubmit)}

   </div>
}

const englishSubmitPreprintForm =
   <HookSubmitPreprint d={translatablesEnglish.translatables} siteMap={englishMarXivMinimalSitemap} ></HookSubmitPreprint>;

const domContainer = document.getElementById('reactSubmitForm');
if (domContainer) {
   render(englishSubmitPreprintForm, domContainer);
}
