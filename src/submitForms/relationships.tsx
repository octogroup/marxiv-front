import * as React from 'react';
import { Translatables, SelectOption, makeRelationshipOptions, WorkRelationship } from '@octogroup/marxiv-common';
import useForm from 'react-hook-form'
import Select from 'react-select';

import { validateReqPublisherDOI } from '../helpers/validations';
import { FormErrorMessage, hastToReact, getDefaultValue, Relationship, createID } from '../helpers/formHelpers';

/**
 * Relationships between works
 */
export interface RelationshipState extends Relationship {
}

const initialRelationship: Relationship = {
   doi: '',
   type: '',
   id: createID(),
};

/**
 * Add the currentRelationship to the parent's Relationship[]
 */
const addCurrentToParent = (current: Relationship, existing: Relationship[] | undefined): Relationship[] => {
   if (existing && existing.length > 0) {
      // Remove the currentRelationship from the existing array, so we don't duplicate it.
      const withoutCurrent: Relationship[] = existing.filter(v => v.id !== current.id);
      // Add the currentRelationship to the end of the array
      const newRelationships = [ ...withoutCurrent, current ];
      return newRelationships
   }
   else {
      // If there are no existing contributors, simply return the current.
      return [current]
   }
}

export const HookRelationships = function ({ d, register, setValue, getValues }: { d: Translatables, register: any, setValue: any, getValues: any } ) {
   // Setup the local form
   const methods = useForm<Relationship>({
      mode: 'onBlur',
      reValidateMode: 'onChange',
      defaultValues: initialRelationship
   });

   const localRegister = methods.register;
   const localErrors = methods.errors;
   const localSetValue = methods.setValue;
   const localGetValues = methods.getValues;
   const localHandleSubmit = methods.handleSubmit;

   const resetRelationship = (): void => {
      localSetValue('doi', '');
      localSetValue('type', '');
      localSetValue('id', createID());
   };
   const resetToRelationship = (relationship: Relationship): void => {
      localSetValue('doi', relationship.doi);
      localSetValue('type', relationship.type);
      localSetValue('id', relationship.id);
   };

   // Register values for React-state controlled data
   React.useEffect(() => {
      localRegister({ name: 'type' }, { required: true } );
      localRegister({ name: 'id' });
      register({ name: 'relationships' });
   }, [localRegister, register]);

   // Set our Select options
   const relationshipOptions = makeRelationshipOptions(d);

   // Create our initial Select selection
   const initialRelationshipOption: SelectOption = getDefaultValue(initialRelationship.type)(relationshipOptions);

   // Set our initial values for React-state controlled data
   const [values, setReactSelectValue] = React.useState({
      selectedRelationshipType: initialRelationshipOption
   });

   const handleRelationshipTypeChange = (selectedRelationshipTypeOption: SelectOption): void => {
      localSetValue('type', selectedRelationshipTypeOption.value as WorkRelationship);
      setReactSelectValue({ selectedRelationshipType: selectedRelationshipTypeOption })
   };

   const onPreviewItemClick = (relationships: Relationship[]) => (event: React.MouseEvent<HTMLDivElement, MouseEvent>): void => {
      const val: string = event.currentTarget.innerText;
      const colonPos = val.indexOf(':');
      const relationshipNumber: number = parseInt(val.slice(0, colonPos), 10);
      let clickedRelationship = relationships[relationshipNumber - 1];
      // To fully reset the form to the ClickedRelationship, we need to manually reset the React.useState values
      setReactSelectValue({
         selectedRelationshipType: getDefaultValue(clickedRelationship.type)(relationshipOptions)
      });
      resetToRelationship(clickedRelationship);
   }

   const onDeleteIconClick = (relationships: Relationship[]) => (event: React.MouseEvent<HTMLDivElement, MouseEvent>): void => {
      console.log("Running onDeleteIconClick.")

      // Prevent this event from bubbling up and triggering the onPreviewItemClick callback
      event.stopPropagation();

      // Determine the Relationship to delete
      const toDeleteRelationshipID: string = event.currentTarget.id;
      const clickedRelationship: Relationship | undefined = relationships.find((value) => value.id === toDeleteRelationshipID);

      // Remove clickedRelationship from relationships[]
      const cleanedRelations: Relationship[] = relationships.filter(item => item !== clickedRelationship);

      console.log("Clicked to delete relationship:")
      console.log(clickedRelationship);

      console.log("Cleaned relationship:");
      console.log(cleanedRelations);

      // Save the cleaned values to the parent
      setValue('relationships', cleanedRelations);

      // To fully reset the form, we need to manually reset the React.useState values
      setReactSelectValue({
         selectedRelationshipType: initialRelationshipOption
      });
      // We need to manually create a new ID, since initialRelationship has a static ID.
      resetRelationship();
   };

   function makePreviewItems(relationships: Relationship[]): JSX.Element[] {
      let formattedItems: JSX.Element[] = new Array;
      for (let i = 0; i < relationships.length; i++) {
         formattedItems.push(<div className='savedInput flex all-parent-row' key={i} onClick={onPreviewItemClick(relationships)} >
            {(i + 1).toString() + ': ' + relationships[i].doi}
            <span>
               <img src='/../../images/icons/times.svg' className='deleteIcon' id={relationships[i].id} onClick={onDeleteIconClick(relationships)} />
            </span>
         </div>);
      }
      return formattedItems
   }

   function RelationshipPreviewBar({ d }: { d: Translatables }): JSX.Element {
      let content: Relationship[] = getValues({ nest: true }).relationships;
      if (content && content.length > 0) {
         const formattedData: JSX.Element[] = makePreviewItems(content);
         return <div className='inputTextMulti tooltip flex all-parent-row' id='relationshipList' >
            {formattedData}
         </div>
      }
      else {
         // Fill in default text
         return <div className='inputTextMulti tooltip flex all-parent-row' id='relationshipList'>
            <div className='emptyList'>
               {d.forms.submit.relationships.previewDefault}
            </div>
         </div>
      }
   }
   /**
    * Save a new relationship to the Relationships array
    */
   const addRelationship = (): void => {
      const currentVals = localGetValues({ nest: true });
      const currentRelationship: Relationship = {
         doi: currentVals.doi,
         type: currentVals.type,
         id: currentVals.id
      };
      const relationshipArray: Relationship[] = getValues({ nest: true }).relationships;

      const newRelationships = addCurrentToParent(currentRelationship, relationshipArray);

      console.log("Current values:");
      console.log(currentVals);
      console.log("New relationships:");
      console.log(newRelationships);

      setValue('relationships', newRelationships);

      // To fully reset the form, we need to manually reset the React.useState values
      setReactSelectValue({
         selectedRelationshipType: initialRelationshipOption
      });
      // We need to manually create a new ID, since initialRelationship has a static ID.
      resetRelationship();
   }

   /**
    * Render the content
    */
   return <div className='form wrapper'>
      <div className='title flex all-child-span1'>
         {d.forms.submit.relationships.formTitle}
      </div>
      <div className='content'>

         <div className='field'>
            <div
               className='description'
               dangerouslySetInnerHTML={hastToReact(d.forms.submit.relationships.description)}
            />  
         </div>

         <div id='relationshipList'>

            <RelationshipPreviewBar
               d={d}
            />

            <div className='group'>
               <div className="groupLabel">
                  {d.forms.submit.relationships.relationship.formTitle}
               </div>

               <div className='field input shortText flex all-parent-row center'>
                  <label htmlFor='relatedDOI'>
                     {d.forms.submit.relationships.relationship.doi.fieldTitle}
                  </label>
                  <input
                     ref={localRegister({
                        validate: validateReqPublisherDOI(d),
                     })}
                     placeholder={d.forms.submit.relationships.relationship.doi.default}
                     name='doi'
                     id='relatedDOI'
                     type='text'
                     className={localErrors.relatedDOI ? 'invalid' : undefined}
                  />
               </div>
               {localErrors.doi && localErrors.doi.message ? (<FormErrorMessage>{localErrors.doi.message}</FormErrorMessage>) : null}

               <div className='field selectList flex all-parent-row center'>
                  <label htmlFor='relationshipType'>
                     {d.forms.submit.relationships.relationship.typeOfRelationship.fieldTitle}
                  </label>
                  <Select
                     options={relationshipOptions}
                     isMulti={false}
                     isSearchable={false}
                     name="type"
                     id='relationshipType'
                     ref={localRegister}
                     value={values.selectedRelationshipType}
                     onChange={handleRelationshipTypeChange}
                  />
               </div>
               {localErrors.type ? (<FormErrorMessage>{d.forms.messages.errors.relationshipType}</FormErrorMessage>) : null}
            
            </div>

            <div className='flex all-parent-row center'>
               <button type='button' className='addRelationship button orange flex all-child-span1' onClick={localHandleSubmit(addRelationship)}>
                  {d.forms.submit.relationships.relationship.add}
               </button>
            </div>

         </div>
      </div>
   </div>
}
