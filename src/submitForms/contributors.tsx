import * as React from 'react';
import { Translatables, makeNameStyleOptions, makeContributorRoleOptions, SelectOption, NameStyle, ContribRole } from '@octogroup/marxiv-common';
import useForm from 'react-hook-form'
import Select from 'react-select';

import Collapsible from 'react-collapsible';

import { validateORCID, validateRequiredText, validateOptionalText, validateOptionalEmailAddress, validateContribs } from '../helpers/validations';
import { FormErrorMessage, hastToReact, Contributor, getDefaultValue, createID } from '../helpers/formHelpers';

/**
 * Contribututors
 */
const initialContrib: Contributor = {
   surname: '',
   nameStyle: 'western',
   role: 'author',
   sequence: 'first',
   givenName: '',
   email: '',
   affiliation: '',
   orcid: '',
   suffix: '',
   isOrg: undefined,
   id: createID(),
   order: '1'
};

/**
 * Add the currentContrib to the parent's Contributor[]
 */
const sortOrder = (contribs: Contributor[]): Contributor[] => {
   return contribs.sort((a: Contributor, b: Contributor) => Number(a.order) - Number(b.order));
}

const correctOrder = (contribs: Contributor[]): Contributor[] => {
   return contribs.map((val: Contributor, ix: number) => ({
      ...val,
      order: (ix + 1).toString()
   }))
}

const addCurrentToParent = (current: Contributor, existing: Contributor[] | undefined): Contributor[] => {
   if (existing && existing.length > 0) {
      // Get the order of the currentContrib
      const currentOrder = Number(current.order);
      // Remove the currentContrib from the existing array, so we don't duplicate it.
      const withoutCurrent: Contributor[] = existing.filter(v => v.id !== current.id);
      // Sort & re-order the list
      const sortedWithout: Contributor[] = correctOrder(sortOrder(withoutCurrent));
      console.log('Sorted:');
      console.log(sortedWithout);
      // Filter the list into the Head and Tail
      const withoutCurrentHead: Contributor[] = sortedWithout.filter(v => Number(v.order) < currentOrder);
      const withoutCurrentTail: Contributor[] = sortedWithout.filter(v => Number(v.order) >= currentOrder);
      // Add the currentContrib between the Head and Tail
      const newContributors: Contributor[] = [
         ...withoutCurrentHead,
         current,
         ...withoutCurrentTail
      ];
      // Set order to reflect the current position in the array
      const orderedContribs: Contributor[] = correctOrder(newContributors);
      console.log('Ordered:');
      console.log(orderedContribs);

      return orderedContribs
   }
   else {
      // If there are no existing contributors, simply return the current.
      return [current]
   }
}

export const HookContribs = function ({ d, register, getValues, setValue }: { d: Translatables, register: any, getValues: any, setValue: any }) {
   // Retrieve all hook methods from the parent form
   const methods = useForm<Contributor>({
      mode: 'onBlur',
      reValidateMode: 'onChange',
      defaultValues: {
         ...initialContrib,
         order: (getValues({ nest: true }).contributors.length + 1).toString()
      }
   });

   const localRegister = methods.register;
   const localErrors = methods.errors;
   const localGetValues = methods.getValues;
   const localSetValue = methods.setValue;
   const localHandleSubmit = methods.handleSubmit;

   // Register values for React-state controlled data
   React.useEffect(() => {
      register({ name: 'contributors' }, { validate: validateContribs(d.forms.messages.errors.contribsRequired) });
   }, [register]);

   React.useEffect(() => {
      localRegister({ name: 'nameStyle' });
      localRegister({ name: 'role' });
      localRegister({ name: 'id' });
      localRegister({ name: 'order' });
      localRegister({ name: 'isOrg' });
   }, [localRegister]);

   // Set our Select options
   const nameStyleOptions = makeNameStyleOptions(d.forms.submit);
   const contributorRoleOptions = makeContributorRoleOptions(d.forms.submit);
   function makeContribOrderOptions(): SelectOption[] {
      let contribs: Contributor[] = getValues({ nest: true }).contributors;
      if (!contribs || contribs.length < 1) {
         return [{
            label: '1',
            value: '1'
         }]
      }
      else {
         let opts: SelectOption[] = new Array;
         for (let i = 0; i < contribs.length; i++) {
            opts.push({
               label: (i + 1).toString(),
               value: (i + 1).toString(),
            })
         }
         return opts
      }
   };
   const orderOptions = makeContribOrderOptions();

   // Create our initial Select selection
   const initialNameStyleOption: SelectOption = getDefaultValue(initialContrib.nameStyle)(nameStyleOptions);
   const initialRoleOption: SelectOption = getDefaultValue(initialContrib.role)(contributorRoleOptions);
   const initialOrderOption: SelectOption = orderOptions[0];

   // Set our initial values for React-state controlled data
   const [values, setReactSelectValue] = React.useState({
      selectedNameStyleOption: initialNameStyleOption,
      selectedRoleOption: initialRoleOption,
      selectedOrderOption: initialOrderOption,
      id: initialContrib.id,
      isOrg: initialContrib.isOrg,
   });

   const handleNameStyleChange = (selectedNameStyleOption: SelectOption): void => {
      localSetValue('nameStyle', selectedNameStyleOption.value as NameStyle);
      setReactSelectValue({ ...values, selectedNameStyleOption: selectedNameStyleOption })
   };

   const handleRoleOptionChange = (selectedRoleOption: SelectOption): void => {
      localSetValue('role', selectedRoleOption.value as ContribRole);
      setReactSelectValue({ ...values, selectedRoleOption: selectedRoleOption })
   };

   const handleOrderOptionChange = (selectedOrderOption: SelectOption): void => {
      localSetValue('order', selectedOrderOption.value);
      setReactSelectValue({ ...values, selectedOrderOption: selectedOrderOption })
   };

   const handleIsOrgChange = (ev: React.ChangeEvent<HTMLInputElement>): void => {
      const isChecked = ev.currentTarget.checked;
      console.log("isOrg is " + isChecked);
      if (isChecked) {
         localSetValue('isOrg', 'on');
         setReactSelectValue({ ...values, isOrg: 'on' })
      }
      else {
         localSetValue('isOrg', undefined);
         setReactSelectValue({ ...values, isOrg: undefined })
      }
   }

   // Since we can't rely on the built-in reset(), roll our own.
   const resetToContrib = (custom: Contributor): void => {
      localSetValue('surname', custom.surname);
      localSetValue('nameStyle', custom.nameStyle);
      localSetValue('role', custom.role);
      localSetValue('sequence', custom.sequence);
      localSetValue('givenName', custom.givenName);
      localSetValue('email', custom.email);
      localSetValue('affiliation', custom.affiliation);
      localSetValue('orcid', custom.orcid);
      localSetValue('suffix', custom.suffix);
      localSetValue('isOrg', custom.isOrg);
      localSetValue('id', custom.id);
      localSetValue('order', custom.order)
   };
   const resetContrib = (order: string): void => {
      localSetValue('surname', '');
      localSetValue('nameStyle', 'western');
      localSetValue('role', 'author');
      localSetValue('sequence', 'additional');
      localSetValue('givenName', undefined);
      localSetValue('email', undefined);
      localSetValue('affiliation', undefined);
      localSetValue('orcid', undefined);
      localSetValue('suffix', undefined);
      localSetValue('isOrg', undefined);
      localSetValue('id', createID());
      localSetValue('order', order);
   };

   const onPreviewItemClick = (contribs: Contributor[]) => (event: React.MouseEvent<HTMLDivElement, MouseEvent>): void => {
      const val: string = event.currentTarget.innerText;
      const colonPos = val.indexOf(':');
      const contribNumber: number = parseInt(val.slice(0, colonPos), 10);
      let clickedContrib = contribs[contribNumber - 1];
      // To fully reset the form to the ClickedContrib, we need to manually reset the React.useState values
      setReactSelectValue({
         selectedNameStyleOption: getDefaultValue(clickedContrib.nameStyle)(nameStyleOptions),
         selectedRoleOption: getDefaultValue(clickedContrib.role)(contributorRoleOptions),
         selectedOrderOption: {
            label: contribNumber.toString(),
            value: contribNumber.toString()
         },
         id: clickedContrib.id,
         isOrg: clickedContrib.isOrg
      });
      resetToContrib(clickedContrib);
   }

   const onDeleteIconClick = (contribs: Contributor[]) => (event: React.MouseEvent<HTMLDivElement, MouseEvent>): void => {
      console.log("Running onDeleteIconClick.")

      // Prevent this event from bubbling up and triggering the onPreviewItemClick callback
      event.stopPropagation();

      // Determine the Contributor to delete
      const toDeleteContribID: string = event.currentTarget.id;
      const clickedContrib: Contributor | undefined = contribs.find((value) => value.id === toDeleteContribID);

      // Remove clickedContrib from contribs[]
      const cleanedContribs: Contributor[] = contribs.filter(item => item !== clickedContrib);
      const cleanedAndOrdered: Contributor[] = correctOrder(cleanedContribs);

      console.log("Clicked to delete contrib:")
      console.log(clickedContrib);

      console.log("Cleaned and ordered contribs:");
      console.log(cleanedAndOrdered);
      // Save the cleaned values to the parent
      setValue('contributors', cleanedAndOrdered);

      // To fully reset the form, we need to manually reset the React.useState values
      setReactSelectValue({
         selectedNameStyleOption: initialNameStyleOption,
         selectedRoleOption: initialRoleOption,
         selectedOrderOption: {
            label: (cleanedAndOrdered.length + 1).toString(),
            value: (cleanedAndOrdered.length + 1).toString()
         },
         id: createID(),
         isOrg: initialContrib.isOrg
      });
      // We need to manually create a new ID, since initialContrib has a static ID.
      resetContrib( (cleanedAndOrdered.length + 1).toString() );
   };

   function makePreviewItems(contribs: Contributor[]): JSX.Element[] {
      let formattedItems: JSX.Element[] = new Array;
      for (let i = 0; i < contribs.length; i++) {
         formattedItems.push(<div className='savedInput flex all-parent-row' key={i} onClick={onPreviewItemClick(contribs)} >
            {(i + 1).toString() + ': ' + contribs[i].surname}
            <span>
               <img src='/../../images/icons/times.svg' className='deleteIcon' id={contribs[i].id} onClick={onDeleteIconClick(contribs)} />
            </span>
         </div>);
      }
      return formattedItems
   }

   function ContribPreviewBar({ d }: { d: Translatables }): JSX.Element {
      let content: Contributor[] = getValues({ nest: true }).contributors;
      if (content && content.length > 0) {
         const formattedData: JSX.Element[] = makePreviewItems(content);
         return <div className='inputTextMulti tooltip flex all-parent-row' id='contribList' >
            {formattedData}
         </div>
      }
      else {
         // Fill in default text
         return <div className='inputTextMulti tooltip flex all-parent-row' id='contribList'>
            <div className='emptyList'>
               {d.forms.submit.contributors.previewDefault}
            </div>
         </div>
      }
   }
   /**
    * Save a new contributor to the Contributors array
    */
   const addContrib = (): void => {
      const currentContrib: Contributor = localGetValues({ nest: true });
      const contribArray: Contributor[] = getValues({ nest: true }).contributors;

      const newOrderedContribs = addCurrentToParent(currentContrib, contribArray);

      console.log("Current contrib:");
      console.log(currentContrib);
      console.log("New and ordered contribs:");
      console.log(newOrderedContribs);

      setValue('contributors', newOrderedContribs);

      // To fully reset the form, we need to manually reset the React.useState values
      setReactSelectValue({
         selectedNameStyleOption: initialNameStyleOption,
         selectedRoleOption: initialRoleOption,
         selectedOrderOption: {
            label: (newOrderedContribs.length + 1).toString(),
            value: (newOrderedContribs.length + 1).toString()
         },
         id: createID(),
         isOrg: initialContrib.isOrg
      });
      // We need to manually create a new ID, since initialContrib has a static ID.
      resetContrib( (newOrderedContribs.length + 1).toString() );
   }

   return <div className='form wrapper'>
      <div className='title flex all-child-span1'>
         {d.forms.submit.contributors.formTitle}
      </div>
      <div className='content'>
         <div id='authorship'>
            <div className='description' dangerouslySetInnerHTML={hastToReact(d.forms.submit.contributors.description)} />
            <ContribPreviewBar
               d={d}
            />
            <div className='group'>
               <div className="groupLabel">
                  {d.forms.submit.contributors.contributor.formTitle}
               </div>
               {values.isOrg === 'on' ? null : (
                  <div className='field input shortText flex all-parent-row center'>
                     <label htmlFor='contribGiven'>
                        {d.forms.join.fields.givenName}
                     </label>
                     <input
                        ref={localRegister({
                           validate: validateOptionalText(d.forms.messages.errors.givenName)
                        })}
                        placeholder={d.forms.join.fields.givenNameDefault}
                        name='givenName'
                        id='contribGiven'
                        type='text'
                        className={localErrors.org ? 'invalid' : undefined}
                     />
                  </div>
               )}
               {localErrors.contribGiven && localErrors.contribGiven.message ? (<FormErrorMessage>{localErrors.contribGiven.message}</FormErrorMessage>) : null}
               <div className='field input shortText flex all-parent-row center'>
                  <label htmlFor='contribSurname'>
                     {localGetValues({ nest: true }).isOrg === 'on' ? d.forms.submit.contributors.organization.fieldTitle : d.forms.join.fields.surName}
                  </label>
                  <input
                     ref={localRegister({
                        validate: validateRequiredText(d.forms.messages.errors.surname)
                     })}
                     placeholder={localGetValues({ nest: true }).isOrg === 'on' ? d.forms.submit.contributors.organization.default : d.forms.join.fields.surName}
                     name='surname'
                     id='contribSurname'
                     type='text'
                     className={localErrors.org ? 'invalid' : 'required'}
                  />
               </div>
               {localErrors.contribSurname && localErrors.contribSurname.message ? (<FormErrorMessage>{localErrors.contribSurname.message}</FormErrorMessage>) : null}
               <div className='field input shortText flex all-parent-row center'>
                  <label htmlFor='contribEmail'>
                     {d.forms.join.fields.emailAddress}
                  </label>
                  <input
                     ref={localRegister({
                        validate: validateOptionalEmailAddress(d)
                     })}
                     placeholder={d.forms.join.fields.emailAddressDefault}
                     name='email'
                     id='contribEmail'
                     type='email'
                     className={localErrors.org ? 'invalid' : undefined}
                  />
               </div>
               {localErrors.contribEmail && localErrors.contribEmail.message ? (<FormErrorMessage>{localErrors.contribEmail.message}</FormErrorMessage>) : null}
               {values.isOrg === 'on' ? null : (
                  <div className='field input shortText flex all-parent-row center'>
                     <label htmlFor='contribAffiliation'>
                        {d.forms.submit.contributors.contributor.affiliation.fieldTitle}
                     </label>
                     <input
                        ref={localRegister({
                           validate: validateOptionalText(d.forms.messages.errors.generalInvlidShortText)
                        })}
                        placeholder={d.forms.submit.contributors.contributor.affiliation.default}
                        name='affiliation'
                        id='contribAffiliation'
                        type='text'
                        className={localErrors.org ? 'invalid' : undefined}
                     />
                  </div>
               )}
               {localErrors.contribAffiliation && localErrors.contribAffiliation.message ? (<FormErrorMessage>{localErrors.contribAffiliation.message}</FormErrorMessage>) : null}
               {values.isOrg === 'on' ? null : (
                  <div className='field input shortText flex all-parent-row center'>
                     <label htmlFor='contribORCID'>
                        {d.forms.join.fields.orcid}
                     </label>
                     <input
                        ref={localRegister({
                           validate: validateORCID(d)
                        })}
                        placeholder={d.forms.join.fields.orcidDefault}
                        name='orcid'
                        id='contribORCID'
                        type='text'
                        className={localErrors.org ? 'invalid' : undefined}
                     />
                  </div>
               )}
               {localErrors.contribORCID && localErrors.contribORCID.message ? (<FormErrorMessage>{localErrors.contribORCID.message}</FormErrorMessage>) : null}
               <div className='badge blue flex all-parent-row center'>
                  <Collapsible
                     trigger={<div className='collapsible-title'>{d.forms.submit.basic.optional.fieldTitle}</div>}
                     classParentString='collapsible'
                  >
                     <div className='flex all-child-span100'>
                        <div className='field input shortText flex all-parent-row center'>
                           <label htmlFor='suffix'>
                              {d.forms.submit.contributors.contributor.suffix.fieldTitle}
                           </label>
                           <input
                              ref={localRegister({
                                 validate: validateOptionalText(d.forms.messages.errors.generalInvlidShortText)
                              })}
                              placeholder={d.forms.submit.contributors.contributor.suffix.default}
                              name='suffix'
                              id='suffix'
                              type='text'
                              className={localErrors.org ? 'invalid' : undefined}
                           />
                        </div>
                        {localErrors.suffix && localErrors.suffix.message ? (<FormErrorMessage>{localErrors.suffix.message}</FormErrorMessage>) : null}
                        <div className='field selectList flex all-parent-row center'>
                           <label htmlFor='nameStyle'>
                              {d.forms.submit.contributors.contributor.nameStyle.fieldTitle}
                           </label>
                           <Select
                              options={nameStyleOptions}
                              isMulti={false}
                              isSearchable={false}
                              name="nameStyle"
                              id='nameStyle'
                              ref={localRegister}
                              value={values.selectedNameStyleOption}
                              onChange={handleNameStyleChange}
                           />
                        </div>
                        <div className='field selectList flex all-parent-row center'>
                           <label htmlFor='role'>
                              {d.forms.submit.contributors.contributor.role.fieldTitle}
                           </label>
                           <Select
                              options={contributorRoleOptions}
                              isMulti={false}
                              isSearchable={false}
                              id='role'
                              name="role"
                              ref={localRegister}
                              value={values.selectedRoleOption}
                              onChange={handleRoleOptionChange}
                           />
                        </div>
                        <div className='field selectList flex all-parent-row center'>
                           <label htmlFor='order'>
                              {d.forms.submit.contributors.contributor.order}
                           </label>
                           <Select
                              options={orderOptions}
                              isMulti={false}
                              isSearchable={false}
                              id='order'
                              name="order"
                              ref={localRegister}
                              value={values.selectedOrderOption}
                              onChange={handleOrderOptionChange}
                           />
                        </div>
                        <div className='flex all-parent-row center'>
                           <div className='radio'>
                              <input
                                 type='radio'
                                 id='first'
                                 name='sequence'
                                 value='first'
                                 ref={localRegister}
                                 defaultChecked={true}
                              />
                              <label htmlFor='first'>
                                 {d.forms.submit.contributors.contributor.firstAuthor}
                              </label>
                           </div>
                           <div className='radio'>
                              <input
                                 type='radio'
                                 id='additional'
                                 name='sequence'
                                 value='additional'
                                 ref={localRegister}
                              />
                              <label htmlFor='additional'>
                                 {d.forms.submit.contributors.contributor.additionalAuthor}
                              </label>
                           </div>
                        </div>
                        <div className='field input flex all-parent-row center'>
                           <label htmlFor='isOrg'>
                              {d.forms.submit.contributors.organization.isOrg}
                           </label>
                           <input
                              type='checkbox'
                              id='isOrg'
                              name='isOrg'
                              value='on'
                              ref={localRegister}
                              onChange={handleIsOrgChange}
                           />
                        </div>
                     </div>
                  </Collapsible>
               </div>
               <div className='flex all-parent-row center'>
                  <button type='button' className='addAuthor button orange flex all-child-span1' onClick={localHandleSubmit(addContrib)}>
                     {d.forms.submit.contributors.contributor.add}
                  </button>
               </div>
            </div>
         </div>
         <div className='errorContainer'>
            { localErrors.contributors ? (<FormErrorMessage>{d.forms.messages.errors.contribsRequired}</FormErrorMessage>) : null }
         </div>
      </div>
   </div>
}
