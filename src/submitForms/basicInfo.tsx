import * as React from 'react';
import { Translatables, SelectOption, makeLanguageOptions } from '@octogroup/marxiv-common';
import Select from 'react-select';

import { validateTitle, validateSubtitle, validateOptPublisherDOI, validateYear, validateMonth, validateDay, validateDescription, validateReqPublisherDOI } from '../helpers/validations';
import { FormErrorMessage, getDefaultValue, } from '../helpers/formHelpers';

/**
 * Basic Information for Posted Content
 */
export interface PostedContentState {
   title: string
   language: string
   peerReviewed: 'peerReviewed' | 'notPeerReviewed' | ''
   subtitle: string
   publisherDOI: string
   description: string
   originalPublicationYear: string
   originalPublicationMonth: string
   originalPublicationDay: string
}

export const HookBasicInfo = function ({ d, initialValues, isPostprint, register, setValue, errors }: { d: Translatables, initialValues: PostedContentState, isPostprint: boolean, register: any, setValue: any, errors: any }) {
   // Register values for React-state controlled data
   React.useEffect(() => {
      register({ name: 'language' });
   }, [register]);

   // Set our Select options
   const languageSelectOptions: SelectOption[] = makeLanguageOptions(d)(initialValues.language);
   const initialLangaugeSelectionOption: SelectOption = getDefaultValue(initialValues.language)(languageSelectOptions);

   // Set our initial values for React-state controlled data
   const [values, setReactSelectValue] = React.useState({
      selectedLanguageOption: initialLangaugeSelectionOption
   });

   const handleLanguageChange = (selectedLanguageOption: SelectOption): void => {
      setValue('language', selectedLanguageOption.value);
      setReactSelectValue({ selectedLanguageOption: selectedLanguageOption })
   };

   const setPublisherDOIClassName = (isPostprint: boolean): (err: boolean) => string | undefined => {
      if (isPostprint) {
         return (err: boolean) => {
            if (err) {
               return 'invalid'
            }
            else {
               return 'required'
            }
         }
      }
      else {
         return (err: boolean) => {
            if (err) {
               return 'invalid'
            }
            else {
               return undefined
            }
         }
      }
   };

   const makeClass = setPublisherDOIClassName(isPostprint);

   /**
    * Render the content
    */
   return <div id='basicInformationPostedContent'>
      <div className='form wrapper'>
         <div className='title flex all-child-span1'>
            {d.forms.submit.basic.formTitle}
         </div>
         <div className='content'>

            <div className='field input shortText flex all-parent-row center'>
               <label htmlFor='workTitle'>
                  {d.forms.submit.basic.title.fieldTitle}
               </label>
               <input
                  ref={register({
                     validate: validateTitle(d)
                  })}
                  placeholder={d.forms.submit.basic.title.default}
                  name='title'
                  id='workTitle'
                  type='text'
                  className={errors.title && errors.title.message ? 'invalid' : 'required'}
               />
            </div>
            {errors.title && errors.title.message ? (<FormErrorMessage>{errors.title.message}</FormErrorMessage>) : null}

            <div className='field input shortText flex all-parent-row center'>
               <label htmlFor='workSubtitle'>
                  {d.forms.submit.basic.optional.subtitle.fieldTitle}
               </label>
               <input
                  ref={register({
                     validate: validateSubtitle(d),
                  })}
                  placeholder={d.forms.submit.basic.optional.subtitle.default}
                  name='subtitle'
                  id='workSubtitle'
                  type='text'
                  className={errors.subtitle && errors.subtitle.message ? 'invalid' : undefined}
               />
            </div>
            {errors.subtitle && errors.subtitle.message ? (<FormErrorMessage>{errors.subtitle.message}</FormErrorMessage>) : null}

            <div className='field input shortText flex all-parent-row center'>
               <label htmlFor='publisherDOI'>
                  {d.forms.submit.basic.publisherDOI.fieldTitle}
               </label>
               <input
                  ref={register({
                     validate: isPostprint ? validateReqPublisherDOI(d) : validateOptPublisherDOI(d)
                  })}
                  placeholder={d.forms.submit.basic.publisherDOI.default}
                  name='publisherDOI'
                  id='publisherDOI'
                  type='text'
                  className={errors.publisherDOI && errors.publisherDOI.message ? makeClass(true) : makeClass(false)}
               />
            </div>
            {errors.publisherDOI && errors.publisherDOI.message ? (<FormErrorMessage>{errors.publisherDOI.message}</FormErrorMessage>) : null}

            <div className='field selectList flex all-parent-row center'>
               <label htmlFor='language'>
                  {d.forms.submit.basic.optional.language.fieldTitle}
               </label>
               <Select
                  options={languageSelectOptions}
                  isMulti={false}
                  isSearchable={false}
                  name="language"
                  id='language'
                  ref={register}
                  value={values.selectedLanguageOption}
                  onChange={handleLanguageChange}
               />
            </div>

            <div className='peerReviewQuestion field'>
               <div className='description'>
                  {d.forms.submit.basic.peerReview.fieldTitle}
               </div>
               <div className='flex all-parent-row center'>
                  <div className='radio'>
                     <input
                        id='peerReviewed'
                        name='peerReviewed'
                        value='peerReviewed'
                        type='radio'
                        ref={register({
                           required: true
                        })}
                     />
                     <label htmlFor='peerReviewed'>
                        {d.forms.submit.basic.peerReview.yes}
                     </label>
                  </div>
                  <div className='radio'>
                     <input
                        id='notPeerReviewed'
                        name='peerReviewed'
                        value='notPeerReviewed'
                        type='radio'
                        ref={register({
                           required: true
                        })}
                     />
                     <label htmlFor='notPeerReviewed'>
                        {d.forms.submit.basic.peerReview.no}
                     </label>
                  </div>
               </div>
            </div>


            <div className='group flex all-parent-column'>
               <div className='groupLabel flex all-child-span1'>
                  {d.forms.submit.basic.originalPublicationDate.fieldTitle}
               </div>
               <div className='fields flex all-child-span1'>

                  <div className='field input shortText flex all-parent-row center'>
                     <label htmlFor='originalPublicationYear'>
                        {d.forms.submit.basic.originalPublicationDate.year.fieldTitle}
                     </label>
                     <input
                        ref={register({
                           validate: validateYear(d.forms.messages.errors.year)
                        })}
                        placeholder={d.forms.submit.basic.originalPublicationDate.year.default}
                        name='originalPublicationYear'
                        id='originalPublicationYear'
                        type='number'
                        className={errors.year && errors.year.message ? 'invalid' : 'required'}
                     />
                  </div>
                  {errors.year && errors.year.message ? (<FormErrorMessage>{errors.year.message}</FormErrorMessage>) : null}

                  <div className='field input shortText flex all-parent-row center'>
                     <label htmlFor='originalPublicationMonth'>
                        {d.forms.submit.basic.originalPublicationDate.month.fieldTitle}
                     </label>
                     <input
                        ref={register({
                           validate: validateMonth(d.forms.messages.errors.month)
                        })}
                        placeholder={d.forms.submit.basic.originalPublicationDate.month.default}
                        name='originalPublicationMonth'
                        id='originalPublicationMonth'
                        type='number'
                        className={errors.month && errors.month.message ? 'invalid' : undefined}
                     />
                  </div>
                  {errors.month && errors.month.message ? (<FormErrorMessage>{errors.month.message}</FormErrorMessage>) : null}

                  <div className='field input shortText flex all-parent-row center'>
                     <label htmlFor='originalPublicationDay'>
                        {d.forms.submit.basic.originalPublicationDate.day.fieldTitle}
                     </label>
                     <input
                        ref={register({
                           validate: validateDay(d.forms.messages.errors.day)
                        })}
                        placeholder={d.forms.submit.basic.originalPublicationDate.day.default}
                        name='originalPublicationDay'
                        id='originalPublicationDay'
                        type='number'
                        className={errors.day && errors.day.message ? 'invalid' : undefined}
                     />
                  </div>
                  {errors.day && errors.day.message ? (<FormErrorMessage>{errors.day.message}</FormErrorMessage>) : null}

               </div>
            </div>

            <div className='field input longText flex all-parent-column'>
               <label htmlFor='description'>
                  {d.forms.submit.basic.description.fieldTitle}
               </label>
               <textarea
                  ref={register({
                     validate: validateDescription(d.forms)
                  })}
                  placeholder={d.forms.submit.basic.description.default}
                  name='description'
                  id='description'
                  className={errors.description && errors.description.message ? 'invalid' : 'required'}
               />
            </div>
            {errors.description && errors.description.message ? (<FormErrorMessage>{errors.description.message}</FormErrorMessage>) : null}
         </div>
      </div>
   </div>
}
