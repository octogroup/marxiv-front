import * as React from 'react';
import { Translatables, } from '@octogroup/marxiv-common';

/**
 * Support MarXiv is actually vHTML, not a form
 */
export const HookDonation = function ({ d }: { d: Translatables }) {
   /**
    * Render the content
    */
   return <div id='donation' className='form wrapper'>
      <div className='title flex all-child-span1'>
         {d.forms.submit.support.formTitle}
      </div>
      <div className='content'>
         <div className='field'>
            <div className='description'>
               {d.forms.submit.support.p1}
            </div>
            <div className='flex all-parent-column'>
               <div id='paypal' className='flex all-child-span1 center'>
                  <div className='button blue'>
                     <a className='normalWhiteLink' href={d.forms.submit.support.paypalLink} target='_blank'>
                        {d.forms.submit.support.button}
                     </a>
                  </div>
                  <p>
                     {d.forms.submit.support.p2}
                  </p> 
               </div>
            </div>
         </div>
      </div>
   </div>
}
