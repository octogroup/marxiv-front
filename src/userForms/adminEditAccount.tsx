import * as React from 'react';
import * as ReactDOM from "react-dom";
import { Translatables, translatablesEnglish, makeJobOptions, makeCountrySelectOptions, SelectOption, englishMarXivMinimalSitemap } from '@octogroup/marxiv-common';
import { MarXivSitemapMinimum } from '@octogroup/marxiv-common/dist/sitemap';
import useForm from 'react-hook-form';
import Select from 'react-select';
import { useOcto } from '../helpers/graphqlHook'
import gql from 'graphql-tag';

import { validateEmailAddress, validateORCID, cleanHTML, } from '../helpers/validations';
import { FormErrorMessage, getDefaultValue, getDefaultMultipleValues } from '../helpers/formHelpers';

interface UserAccountMutation {
   username: string
   email: string
   surname: string
   given: string | undefined
   resetPassword: boolean | undefined
   org: string | undefined
   job: string[] | undefined
   orcid: string | undefined
   country: string | undefined
   newsletter: 'newsletterYes' | 'newsletterNo' | undefined
}

const HookAdminEditUserAccount = function ({ submitBackend, d, siteMap }: { submitBackend: (u: UserAccountMutation) => void, d: Translatables, siteMap: MarXivSitemapMinimum }) {
   // Determine the User Account to edit
   const browserQuery = window.location.search; // e.g. ?u=nick
   const username = browserQuery.slice(3);

   // Set our initial values
   const initialVals: UserAccountMutation = {
      username: username,
      email: 'nick@octogroup.org',
      resetPassword: true,
      given: 'Nick',
      surname: 'Wehner',
      org: 'OCTO',
      job: [],
      orcid: undefined,
      country: 'unitedstatesofamerica',
      newsletter: 'newsletterYes',
   };

   const methods = useForm<UserAccountMutation>({
      mode: 'onBlur',
      reValidateMode: 'onChange',
      defaultValues: initialVals
   });

   /**
    * Redirect the user to the Admin Dashboard.
    */
   const redirect = (): string => {
      const path: string = window.location.pathname; // "/en/submit/preprint/"
      const lang: string = path.slice(0, 3); // "/en"
      const redirectPath: string = lang + siteMap.admin.localURL;
      return window.location.pathname = redirectPath;
   }

   const register = methods.register
   const errors = methods.errors;
   const onSubmit = methods.handleSubmit(submitBackend);
   const errorMsgs = d.forms.messages.errors;

   const jobOptions = makeJobOptions(d);
   const countryOptions = makeCountrySelectOptions();

   const [values, setReactSelectValue] = React.useState({
      selectedJobOption: getDefaultMultipleValues(initialVals.job)(jobOptions),
      selectedCountryOption: getDefaultValue(initialVals.country)(countryOptions),
   });

   const handleMultiJobChange = (selectedJobOption: SelectOption[]) => {
      let newJobs = [...new Set([...values.selectedJobOption, ...selectedJobOption])];
      setReactSelectValue({ ...values, selectedJobOption: newJobs })
      methods.setValue('job', newJobs.map(x => x.value))
   };
   const handleCountryChange = (selectedCountryOption: SelectOption) => {
      methods.setValue('country', selectedCountryOption.value);
      setReactSelectValue({ ...values, selectedCountryOption: selectedCountryOption })
   };

   // Register the Job and Country fields on each load
   React.useEffect(() => {
      register({ name: 'job' });
      register({ name: 'country' });
   }, [register]);

   /**
    * Render the content.
    */
   return <div id='form'>
      <div className='form wrapper' id='adminEditUserAccount'>
         <div className='title flex all-child-span1'>
            {d.pageMetadata.adminEditAccount.title}
         </div>
         <div className='content'>
            <form onSubmit={onSubmit}>

               <div className='field input shortText flex all-parent-row center'>
                  <label htmlFor='username'>
                     {d.forms.login.usernameOrEmail}
                  </label>
                  <input
                     name="username"
                     ref={methods.register({
                        validate: validateEmailAddress
                     })}
                     placeholder={d.forms.login.usernameOrEmailDefault}
                     type='text'
                     id='username'
                     className={errors.username ? 'invalid' : 'required'}
                  />
               </div>
               {errors.username ? (<FormErrorMessage>{errorMsgs.usernameOrEmail}</FormErrorMessage>) : null}

               <div className='field input email flex all-parent-row center'>
                  <label htmlFor='email'>
                     {d.forms.join.fields.emailAddress}
                  </label>
                  <input
                     ref={methods.register({
                        validate: validateEmailAddress
                     })}
                     placeholder={d.forms.join.fields.emailAddressDefault}
                     name="email"
                     type='email'
                     id='email'
                     className={errors.email ? 'invalid' : 'required'}
                  />
               </div>
               {errors.email ? (<FormErrorMessage>{errorMsgs.usernameOrEmail}</FormErrorMessage>) : null}

               <div className='field input checkbox flex all-parent-row center'>
                  <label htmlFor='resetPassword'>
                     {d.forms.resetPassword.formTitle}
                  </label>
                  <input
                     ref={methods.register}
                     type='checkbox'
                     name="resetPassword"
                     id='resetPassword'
                  />
               </div>
               {errors.resetPassword && errors.resetPassword.message ? (<FormErrorMessage>{errors.resetPassword.message}</FormErrorMessage>) : null}

               <div className='spacer verticalBuffer2' />
               <div className='field input shortText flex all-parent-row center'>
                  <label htmlFor='given'>
                     {d.forms.join.fields.givenName}
                  </label>
                  <input
                     ref={methods.register({
                        required: d.forms.messages.errors.givenName,
                        validate: cleanHTML(d.forms.messages.errors.givenName)
                     })}
                     placeholder={d.forms.join.fields.givenNameDefault}
                     name='given'
                     id='given'
                     type='text'
                     className={errors.given ? 'invalid' : 'required'}
                  />
               </div>
               {errors.given && errors.given.message ? (<FormErrorMessage>{errors.given.message}</FormErrorMessage>) : null}
               <div className='field input shortText flex all-parent-row center'>
                  <label htmlFor='surname'>
                     {d.forms.join.fields.surName}
                  </label>
                  <input
                     ref={methods.register({
                        required: d.forms.messages.errors.surname,
                        validate: cleanHTML(d.forms.messages.errors.surname)
                     })}
                     placeholder={d.forms.join.fields.surNameDefault}
                     name='surname'
                     id='surname'
                     type='text'
                     className={errors.surname ? 'invalid' : 'required'}
                  />
               </div>
               {errors.surname && errors.surname.message ? (<FormErrorMessage>{errors.surname.message}</FormErrorMessage>) : null}
               <div className='field input shortText flex all-parent-row center'>
                  <label htmlFor='org'>
                     {d.forms.join.fields.organization}
                  </label>
                  <input
                     ref={methods.register({
                        validate: cleanHTML(d.forms.messages.errors.generalInvlidShortText)
                     })}
                     placeholder={d.forms.join.fields.organizationDefault}
                     name='org'
                     id='org'
                     type='text'
                     className={errors.org ? 'invalid' : undefined}
                  />
               </div>
               {errors.org && errors.org.message ? (<FormErrorMessage>{errors.org.message}</FormErrorMessage>) : null}
               <div className='field selectListMulti flex all-parent-row center'>
                  <label htmlFor='job'>
                     {d.forms.join.fields.jobFocus}
                  </label>
                  <Select
                     options={jobOptions}
                     isMulti={true}
                     isSearchable={true}
                     id='job'
                     name="job"
                     value={values.selectedJobOption}
                     onChange={handleMultiJobChange}
                  />
               </div>
               <div className='field input shortText flex all-parent-row center'>
                  <label htmlFor='orcid'>
                     {d.forms.join.fields.orcid}
                  </label>
                  <input
                     ref={methods.register({
                        validate: validateORCID(d)
                     })}
                     placeholder={d.forms.join.fields.orcidDefault}
                     name='orcid'
                     id='orcid'
                     type='text'
                  />
               </div>
               <div className='field selectList flex all-parent-row center'>
                  <label htmlFor='country'>
                     {d.forms.join.fields.country}
                  </label>
                  <Select
                     options={countryOptions}
                     isMulti={false}
                     isSearchable={true}
                     id='country'
                     name="country"
                     ref={register}
                     value={values.selectedCountryOption}
                     onChange={handleCountryChange}
                  />
               </div>
               <div className='newsletter flex all-parent-row'>
                  <div className='field inputTitle flex all-child-span2'>
                     {d.forms.dashboards.newsletter}
                  </div>
                  <div className='radioGroup flex all-parent-row flex all-child-span1 center'>
                     <div className='radio'>
                        <input
                           type='radio'
                           id='newsletterYes'
                           value='newsletterYes'
                           name='newsletter'
                           ref={methods.register({
                              required: d.forms.messages.errors.newsletter
                           })}
                        />
                        <label htmlFor='newsletterYes'>
                           {d.commonReplacements.yes}
                        </label>
                     </div>
                     <div className='radio'>
                        <input
                           type='radio'
                           id='newsletterNo'
                           value='newsletterNo'
                           name='newsletter'
                           ref={methods.register({
                              required: d.forms.messages.errors.newsletter
                           })}
                        />
                        <label htmlFor='newsletterNo'>
                           {d.commonReplacements.no}
                        </label>
                     </div>
                  </div>
               </div>
               <div className='form end flex all-parent-row'>
                  <div className='flex all-child-span1 center'>
                     <button type="reset" className='button cancel'>
                        {d.commonReplacements.cancel}
                     </button>
                  </div>
                  <div className='flex all-child-span1 center'>
                     <button type="submit" className='button submit'>
                        {d.commonReplacements.submit}
                     </button>
                  </div>
               </div>
            </form>
         </div>
      </div>

   </div>
}

const englishAdminEditUserAccountForm =
   <HookAdminEditUserAccount
      submitBackend={(data: any) => console.log(data)}
      d={translatablesEnglish.translatables}
      siteMap={englishMarXivMinimalSitemap}
   />;

const domContainer = document.getElementById('reactAdminEditUserAccount');
if (domContainer) {
   ReactDOM.render(englishAdminEditUserAccountForm, domContainer);
}