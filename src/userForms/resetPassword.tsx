import * as React from 'react';
import * as ReactDOM from "react-dom";
import { Translatables, translatablesEnglish, englishMarXivMinimalSitemap } from '@octogroup/marxiv-common';
import { MarXivSitemapMinimum } from '@octogroup/marxiv-common/dist/sitemap';
import useForm from 'react-hook-form'
import { useOcto } from '../helpers/graphqlHook'
import gql from 'graphql-tag';

import { validateEmailAddress, } from '../helpers/validations';
import { FormErrorMessage, } from '../helpers/formHelpers';
import { addEventListeners } from '../scripts/eventListeners';
import { reCAPTCHAv3SiteKey, InvokeReCAPTCHAOptions, rcptaBackoff, } from '../scripts/recaptchaSetup';


interface ResetPasswordMutation {
   username: string
}

const HookPasswordReset = function ({ submitBackend, d, siteMap }: { submitBackend: (u: ResetPasswordMutation) => void, d: Translatables, siteMap: MarXivSitemapMinimum }) {
   const methods = useForm<ResetPasswordMutation>({
      mode: 'onBlur',
      reValidateMode: 'onChange'
   });
   const errors = methods.errors;
   const onSubmit = methods.handleSubmit(submitBackend);
   const errorMsgs = d.forms.messages.errors;

   /**
    * Redirect the user to the homepage.
    */
   const redirect = (): string => {
      const path: string = window.location.pathname; // "/en/submit/preprint/"
      const lang: string = path.slice(0, 3); // "/en"
      const redirectPath: string = lang + siteMap.homepage.localURL;
      return window.location.pathname = redirectPath;
   }

   // Add event listeners just on load
   React.useEffect(() => {
      return addEventListeners();
   }, []);

   /**
    * We need to wait until after the reCAPTCHA script has loaded.
    * Once it has, `grecaptcha` will be defined
    */
   const onLoadResetPassword: InvokeReCAPTCHAOptions = {
      sitekey: reCAPTCHAv3SiteKey,
      action: 'reset'
   };

   return <div id='form'>
      <div className='form wrapper' id='resetPassword'>
         <div className='title flex all-child-span1'>
            {d.forms.resetPassword.formTitle}
         </div>
         <div className='content'>
            <div className='field'>
               <div className='description'>
                  {d.forms.resetPassword.description}
               </div>
            </div>
            <form onSubmit={onSubmit}>
               <div className='field input shortText flex all-parent-row center'>
                  <label htmlFor='username'>
                     {d.forms.login.usernameOrEmail}
                  </label>
                  <input
                     name="username"
                     ref={methods.register({
                        required: true,
                        validate: validateEmailAddress
                     })}
                     placeholder={d.forms.login.usernameOrEmailDefault}
                     type='text'
                     id='username'
                     className={errors.username ? 'invalid' : 'required'}
                  />
               </div>
               {errors.username ? (<FormErrorMessage>{errorMsgs.usernameOrEmail}</FormErrorMessage>) : null}
               <div className='form end flex all-parent-row'>
                  <div className='flex all-child-span1 center'>
                     <button type="reset" className='button cancel'>
                        {d.commonReplacements.cancel}
                     </button>
                  </div>
                  <div className='flex all-child-span1 center'>
                     <button type="submit" className='button submit'>
                        {d.commonReplacements.submit}
                     </button>
                  </div>
               </div>
            </form>
         </div>
      </div>

      {() => rcptaBackoff(onLoadResetPassword)}

   </div>
}

const englishResetPasswordForm =
   <HookPasswordReset
      submitBackend={(data: any) => console.log(data)}
      d={translatablesEnglish.translatables}
      siteMap={englishMarXivMinimalSitemap}
   />;

const domContainer = document.getElementById('reactPasswordReset');
if (domContainer) {
   ReactDOM.render(englishResetPasswordForm, domContainer);
}