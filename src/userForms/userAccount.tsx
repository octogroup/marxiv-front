/**
 * Display user account pages
 */
import * as React from 'react';
import * as ReactDOM from "react-dom";
import { translatablesEnglish, testingContextWithoutTranslatables, Translatables, translateJobOption, MarXivDataWithoutSitemap, GenericAuthenticatedUserData, makeLocalPath, dummyUserData, JobOptionValues, translateCountrySelectOption, makeCountrySelectOptions } from '@octogroup/marxiv-common';

import { reCAPTCHAv3SiteKey, InvokeReCAPTCHAOptions, rcptaBackoff, } from '../scripts/recaptchaSetup';
import { addEventListeners } from '../scripts/eventListeners';

/**
 * We need to wait until after the reCAPTCHA script has loaded.
 * Once it has, `grecaptcha` will be defined
 */
const onLoadUserAccount: InvokeReCAPTCHAOptions = {
   sitekey: reCAPTCHAv3SiteKey,
   action: 'form'
};

const HookUserAccount = function ({ context, d, userData }: { context: MarXivDataWithoutSitemap, d: Translatables, userData: GenericAuthenticatedUserData }) {
   // Add event listeners just on load
   React.useEffect(() => {
      return addEventListeners();
   }, [])

   const jobsToString = (jobs: string[]): string => {
      const jobStrings: string[] = new Array;
      jobs.map((job) => jobStrings.push( translateJobOption(d.forms.join.jobOptions)(job as JobOptionValues) ))
      return jobStrings.join(', ');
   }

   const countrySelectOpts = makeCountrySelectOptions();

   return (<div id='userAccount'>
      <h1 className='title field'>
         {d.forms.userAccount.title}
      </h1>
      <div className='userName field'>
         <span className='label'>
            {d.forms.join.fields.username}
         </span>
         <span className='content'>
            {userData.userName}
         </span>
      </div>
      <div className='emailAddress field'>
         <span className='label'>
            {d.forms.join.fields.emailAddress}
         </span>
         <span className='content'>
            {userData.emailAddress}
         </span>
      </div>
      <div className='givenName field'>
         <span className='label'>
            {d.forms.join.fields.givenName}
         </span>
         <span className='content'>
            {userData.givenName}
         </span>
      </div>
      <div className='surName field'>
         <span className='label'>
            {d.forms.join.fields.surName}
         </span>
         <span className='content'>
            {userData.surname}
         </span>
      </div>
      <div className='organization field'>
         <span className='label'>
            {d.forms.join.fields.organization}
         </span>
         <span className='content'>
            {userData.organization ? userData.organization : null}
         </span>
      </div>
      <div className='jobFocus field'>
         <span className='label'>
            {d.forms.join.fields.jobFocus}
         </span>
         <span className='content'>
            {userData.jobFocus ? jobsToString(userData.jobFocus) : null}
         </span>
      </div>
      <div className='orcid field'>
         <span className='label'>
            {d.forms.join.fields.orcid}
         </span>
         <span className='content'>
            {userData.orcid ? userData.orcid : null}
         </span>
      </div>
      <div className='country field'>
         <span className='label'>
            {d.forms.join.fields.country}
         </span>
         <span className='content'>
            {userData.country ? translateCountrySelectOption(countrySelectOpts)(userData.country) : null}
         </span>
      </div>
      <div className='newsletter field'>
         <span className='label'>
            {d.forms.join.fields.newsletter}
         </span>
         <span className='content'>
            {userData.newsletter ? d.commonReplacements.yes : d.commonReplacements.no}
         </span>
      </div>
      <div className='editWrapper'>
         <a href={makeLocalPath(context.requestedLanguage)(context.minimalSitemap.user.editAccount)} className='button blue'>
            {d.forms.userAccount.editButton}
         </a>
      </div>

      {() => rcptaBackoff(onLoadUserAccount)}

   </div>)
}


const testingEnglishUserAccount: JSX.Element = <HookUserAccount
   context={testingContextWithoutTranslatables}
   d={translatablesEnglish.translatables}
   userData={dummyUserData}
/>;


const domContainer = document.getElementById('reactUserAccount');
if (domContainer) {
   ReactDOM.render(testingEnglishUserAccount, domContainer);
}