/**
 * Admin Moderation Dashboard
 */
import * as React from 'react';
import * as ReactDOM from "react-dom";
import useForm from 'react-hook-form'
import Select from 'react-select';
import { useOcto } from '../helpers/graphqlHook'
import gql from 'graphql-tag';
import Collapsible from 'react-collapsible';
import { Translatables, translatablesEnglish, testingContextWithoutTranslatables, CommonReplacements, WorkData, translateModerationStatus, SupportedLanguage, ActiveWorkData, GenericAuthenticatedUserData, JobOptionValues, makeCountrySelectOptions, makeJobOptions, SelectOption, PostedContentType, LicenseOption, makeLicenseOptions, makeLanguageOptions, translateJobOption, translateCountrySelectOption } from '@octogroup/marxiv-common';
import { StaticWorkText } from '@octogroup/marxiv-common/dist/translatables';
import { ModerationStatus } from '@octogroup/marxiv-common/dist/common';
import { dummyWorksForListing, dummyUserData } from '@octogroup/marxiv-common/dist/testingData';
import { translateWorkTypeBadge, formatOriginalPublicationDate, translateLicenseBadge, OctoWorkTypeAliases, } from '@octogroup/marxiv-common/dist/works';

import { FormErrorMessage, getDefaultValue, getDefaultMultipleValues } from '../helpers/formHelpers';

type DisplayType = 'users' | 'works';

interface AdministrationFilters {
   // USERS
   // Required fields
   userName: string | undefined
   emailAddress: string | undefined
   givenName: string | undefined
   surname: string | undefined
   emailVerified: 'verified' | 'notVerified' | undefined
   // Optional fields
   organization: string | undefined
   jobFocus: JobOptionValues[] | undefined
   orcid: string | undefined
   country: string | undefined
   newsletter: 'newsletterYes' | 'newsletterNo' | undefined
   // WORKS
   workID: string | undefined
   octoWorkType: OctoWorkTypeAliases | undefined
   moderationStatus: ModerationStatus | undefined
   postedContentType: PostedContentType | undefined
   // Basic information
   title: string | undefined
   language: string | undefined
   peerReviewed: 'peerReviewed' | 'notPeerReviewed' | undefined
   publisherDOI: string | undefined
   marxivDOI: string | undefined
   // License & copyright
   license: LicenseOption | undefined
   copyrightHolder: string | undefined
   // Contribututors
   contributorSurname: string | undefined
   submittedBy: string | undefined
}

const HookAdministration = function ({ submitBackend, requestedLanguage, d, works, users }: { submitBackend: (data: AdministrationFilters) => void, requestedLanguage: SupportedLanguage, d: Translatables, works: WorkData[], users: GenericAuthenticatedUserData[] }) {
   const makeOctoWorkTypeOptions = (d: StaticWorkText): SelectOption[] => {
      let opts: SelectOption[] = new Array;
      const badges = d.badges;
      Object.entries(badges).map((value) => {
         let key = value[0];
         let label = value[1];
         opts.push({ label: label, value: key });
      });
      return opts;
   };
   const makeModerationStatusOptions = (d: StaticWorkText): SelectOption[] => {
      let opts: SelectOption[] = new Array;
      const moderation = d.moderation;
      Object.entries(moderation).map((value) => {
         let key = value[0];
         let label = value[1];
         opts.push({ label: label, value: key });
      });
      return opts;
   };
   const makePostedContentTypeOptions = (d: StaticWorkText): SelectOption[] => {
      let opts: SelectOption[] = new Array;
      const badges = d.badges;
      Object.entries(badges).map((value) => {
         let key = value[0];
         let label = value[1];
         if (key === 'preprint') {
            opts.push({ label: label, value: key });
         }
         else if (key === 'workingPaper') {
            opts.push({ label: label, value: key });
         }
         else if (key === 'letter') {
            opts.push({ label: label, value: key });
         }
         else if (key === 'dissertation') {
            opts.push({ label: label, value: key });
         }
         else if (key === 'report') {
            opts.push({ label: label, value: key });
         }
         else if (key === 'other') {
            opts.push({ label: label, value: key });
         }
      });
      return opts;
   }

   /**
    * Setup the form
    */
   const defaultValues: AdministrationFilters = {
      userName: undefined,
      emailAddress: undefined,
      givenName: undefined,
      surname: undefined,
      emailVerified: undefined,
      organization: undefined,
      jobFocus: undefined,
      orcid: undefined,
      country: undefined,
      newsletter: undefined,
      workID: undefined,
      octoWorkType: undefined,
      moderationStatus: undefined,
      postedContentType: undefined,
      title: undefined,
      language: undefined,
      peerReviewed: undefined,
      publisherDOI: undefined,
      marxivDOI: undefined,
      license: undefined,
      copyrightHolder: undefined,
      contributorSurname: undefined,
      submittedBy: undefined,
   };

   const methods = useForm<AdministrationFilters>({
      mode: 'onBlur',
      reValidateMode: 'onChange',
      defaultValues: defaultValues
   });

   const register = methods.register
   const setValue = methods.setValue;
   const reset = methods.reset;
   const onSubmit = methods.handleSubmit(submitBackend);

   /**
    * Use React state for dilineated values
    */
   const emailOptions: SelectOption[] = [
      {
         label: d.commonReplacements.yes,
         value: 'newsletterYes'
      },
      {
         label: d.commonReplacements.no,
         value: 'newsletterNo'
      },
   ];
   const jobOptions: SelectOption[] = makeJobOptions(d);
   const countryOptions: SelectOption[] = makeCountrySelectOptions();
   const newsletterOptions: SelectOption[] = [
      {
         label: d.commonReplacements.yes,
         value: 'newsletterYes'
      },
      {
         label: d.commonReplacements.no,
         value: 'newsletterNo'
      },
   ];
   const octoWorkTypeOptions: SelectOption[] = makeOctoWorkTypeOptions(d.works.static);
   const moderationStatusOptions: SelectOption[] = makeModerationStatusOptions(d.works.static);
   const postedContentTypeOptions: SelectOption[] = makePostedContentTypeOptions(d.works.static);
   const languageOptions: SelectOption[] = makeLanguageOptions(d)(undefined);
   const peerReviewedOptions: SelectOption[] = [
      {
         label: d.forms.submit.basic.peerReview.yes,
         value: 'peerReviewed'
      },
      {
         label: d.forms.submit.basic.peerReview.no,
         value: 'notPeerReviewed'
      },
   ];
   const licenseSelectOptions: SelectOption[] = makeLicenseOptions(d);
   const countrySelectOpts: SelectOption[] = makeCountrySelectOptions();

   const reactSelectInitialValues = {
      displayType: 'works',
      selectedEmailOption: getDefaultValue(defaultValues.emailVerified)(emailOptions),
      selectedJobOption: getDefaultMultipleValues(defaultValues.jobFocus)(jobOptions),
      selectedCountryOption: getDefaultValue(defaultValues.country)(countryOptions),
      selectedNewsletterOption: getDefaultValue(defaultValues.newsletter)(newsletterOptions),
      selectedOctoWorkTypeOption: getDefaultValue(defaultValues.octoWorkType)(octoWorkTypeOptions),
      selectedModerationStatusOption: getDefaultValue(defaultValues.moderationStatus)(moderationStatusOptions),
      selectedPostedContentTypeOption: getDefaultValue(defaultValues.postedContentType)(postedContentTypeOptions),
      selectedLanguageOption: getDefaultValue(defaultValues.language)(languageOptions),
      selectedPeerReviewOption: getDefaultValue(defaultValues.peerReviewed)(peerReviewedOptions),
      selectedLicenseOption: getDefaultValue(defaultValues.license)(licenseSelectOptions),
   };

   const [values, setReactSelectValue] = React.useState(reactSelectInitialValues);

   // Handle Email Change
   const handleEmailChange = (selectedEmailOption: SelectOption) => {
      methods.setValue('emailVerified', selectedEmailOption.value as 'verified' | 'notVerified');
      setReactSelectValue({ ...values, selectedEmailOption: selectedEmailOption });
   };
   const handleMultiJobChange = (selectedJobOption: SelectOption[]) => {
      let newJobs = [...new Set([...values.selectedJobOption, ...selectedJobOption])];
      methods.setValue('job', newJobs.map(x => x.value));
      setReactSelectValue({ ...values, selectedJobOption: newJobs });
   };
   const handleCountryChange = (selectedCountryOption: SelectOption) => {
      methods.setValue('country', selectedCountryOption.value);
      setReactSelectValue({ ...values, selectedCountryOption: selectedCountryOption });
   };
   const handleNewseltterChange = (selectedNewsletterOption: SelectOption) => {
      methods.setValue('newsletter', selectedNewsletterOption.value as 'newsletterYes' | 'newsletterNo');
      setReactSelectValue({ ...values, selectedNewsletterOption: selectedNewsletterOption });
   };
   const handleOctoWorkTypeChange = (selectedOctoWorkTypeOption: SelectOption) => {
      methods.setValue('octoWorkType', selectedOctoWorkTypeOption.value as OctoWorkTypeAliases);
      setReactSelectValue({ ...values, selectedOctoWorkTypeOption: selectedOctoWorkTypeOption });
   };
   const handleModerationStatusChange = (selectedModerationStatusOption: SelectOption) => {
      methods.setValue('moderationStatus', selectedModerationStatusOption.value as ModerationStatus);
      setReactSelectValue({ ...values, selectedModerationStatusOption: selectedModerationStatusOption });
   };
   const handlePostedContentTypeChange = (selectedPostedContentTypeOption: SelectOption) => {
      methods.setValue('postedContentType', selectedPostedContentTypeOption.value as PostedContentType);
      setReactSelectValue({ ...values, selectedPostedContentTypeOption: selectedPostedContentTypeOption });
   };
   const handleLanguageChange = (selectedLanguageOption: SelectOption) => {
      methods.setValue('language', selectedLanguageOption.value);
      setReactSelectValue({ ...values, selectedLanguageOption: selectedLanguageOption });
   };
   const handlePeerReviewChange = (selectedPeerReviewOption: SelectOption) => {
      methods.setValue('peerReviewed', selectedPeerReviewOption.value as 'peerReviewed' | 'notPeerReviewed');
      setReactSelectValue({ ...values, selectedPeerReviewOption: selectedPeerReviewOption });
   };
   const handleLicenseChange = (selectedLicenseOption: SelectOption) => {
      methods.setValue('license', selectedLicenseOption.value as LicenseOption);
      setReactSelectValue({ ...values, selectedLicenseOption: selectedLicenseOption });
   };

   // Register the Job and Country fields on each load
   React.useEffect(() => {
      register({ name: 'emailVerified' });
      register({ name: 'jobFocus' });
      register({ name: 'country' });
      register({ name: 'newsletter' });
      register({ name: 'octoWorkType' });
      register({ name: 'moderationStatus' });
      register({ name: 'postedContentType' });
      register({ name: 'language' });
      register({ name: 'peerReviewed' });
      register({ name: 'license' });
   }, [register]);

   /**
    * Make the components.
    */
   const makeModerationBadge = (d: StaticWorkText) => (work: WorkData): JSX.Element => {
      if (work.moderationStatus === 'rejected' || work.moderationStatus === 'withdrawn') {
         return <div className='badge red center'>
            {translateModerationStatus(d)(work)}
         </div>
      }
      else if (work.moderationStatus === 'pending') {
         return <div className='badge orange center'>
            {translateModerationStatus(d)(work)}
         </div>
      }
      else if (work.marxivDOI) {
         return <div className='badge grey center'>
            {d.doi + ': ' + work.marxivDOI}
         </div>
      }
      else {
         return <div className='badge grey center'>
            {translateModerationStatus(d)(work)}
         </div>
      }
   };

   const makeTypeBadge = (d: StaticWorkText) => (work: WorkData): JSX.Element => <div className='badge orange center'>
      {translateWorkTypeBadge(d)(work)}
   </div>;

   const makeDateBadge = (d: CommonReplacements) => (work: WorkData): JSX.Element => {
      if (work.originalPublicationDate) {
         return <div className='badge blue center'>
            {formatOriginalPublicationDate(d)(work.originalPublicationDate)}
         </div>
      }
      else {
         return <div className='badge blue center'>
            {d.notSet}
         </div>
      }
   };

   const makeLicenseBadge = (d: StaticWorkText) => (work: WorkData): JSX.Element => <div className='badge body center'>
      {translateLicenseBadge(d)(work)}
   </div>;

   const makeSubmitterBadge = (d: CommonReplacements) => (work: WorkData): JSX.Element => <div className='badge grey center'>
      {work.submittedBy ? work.submittedBy : d.notSet}
   </div>;

   const makeModerationLink = (requestedLanguage: SupportedLanguage) => (work: ActiveWorkData): string => {
      const modFormType = (workType: OctoWorkTypeAliases | undefined): string => {
         if (workType) {
            switch (workType) {
               case 'letter':
                  return 'letter';
               case 'oaPaper':
                  return 'oa';
               case 'poster':
                  return 'poster';
               case 'postprint':
                  return 'postprint';
               case 'preprint':
                  return 'preprint';
               case 'reportUnpublished':
                  return 'reportUnpublished';
               case 'thesis':
                  return 'thesis';
               case 'copyrightedPaper':
                  return 'uncopyrighted';
               case 'workingPaper':
                  return 'workingPaper';
               default:
                  return 'preprint';
            }
         }
         else {
            return 'preprint'
         }
      };
      const base: string = '/' + requestedLanguage + '/moderate/';
      const type: string = modFormType(work.octoWorkType);
      const id: string = '?id=' + work.workID;
      return base + type + id;
   }

   const makeModerateButton = (requestedLanguage: SupportedLanguage) => (d: Translatables) => (work: ActiveWorkData): JSX.Element => <a className='button orange' href={makeModerationLink(requestedLanguage)(work)} target='_blank'>
      {d.userMenuText.adminDashboard}
   </a>;

   const makeAdminWorkTeaser = (requestedLanguage: SupportedLanguage) => (d: Translatables) => (work: WorkData) => (i: number): JSX.Element => <div key={i} className='paper teaser'>
      <h2>
         <a href={'/' + requestedLanguage + '/works/' + work.workID} target='_blank'>
            {work.title ? work.title : d.commonReplacements.notSet}
         </a>
      </h2>
      <div className='metadataBar flex all-parent-row'>
         {makeSubmitterBadge(d.commonReplacements)(work)}
         {makeDateBadge(d.commonReplacements)(work)}
         {makeTypeBadge(d.works.static)(work)}
         {makeLicenseBadge(d.works.static)(work)}
         {makeModerationBadge(d.works.static)(work)}
      </div>
      <p className='verticalBuffer2'>
         {work.description ? work.description : d.commonReplacements.notSet}
      </p>
      <p>
         {makeModerateButton(requestedLanguage)(d)(work)}
      </p>
   </div>;

   const combineAdminWorkTeasers = (requestedLanguage: SupportedLanguage) => (d: Translatables) => (works: WorkData[]): JSX.Element[] => {
      let teasers: JSX.Element[] = new Array;
      for (let i = 0; i < works.length; i++) {
         teasers.push(makeAdminWorkTeaser(requestedLanguage)(d)(works[i])(i))
      }
      return teasers
   }

   /**
    * Filtering and sorting menu
    */
   const makeTitle = (title: string): JSX.Element => <div className='title flex all-child-span1'>
      {title}
   </div>;
   const makeMinTrigger = (id: string) => (title: string): JSX.Element => <div className='minWrapper'>
      <div className='collapseBar flex all-parent-row' id={id}>
         {makeTitle(title)}
         <div className='icon center'>
            <img src='/../images/icons/plus-square.svg' className='right maximize' />
         </div>
      </div>
   </div>;

   const makeMaxTrigger = (id: string) => (title: string): JSX.Element => <div className='collapseBar flex all-parent-row' id={id}>
      {makeTitle(title)}
      <div className='icon center'>
         <img src='/../images/icons/minus-square.svg' className='right minimize' />
      </div>
   </div>;

   const makeCollapsibleFilter = (id: string) => (title: string) => (content: JSX.Element): JSX.Element => <Collapsible trigger={makeMinTrigger(id)(title)} triggerWhenOpen={makeMaxTrigger(id)(title)} classParentString='form wrapper'>
      <div className='content'>
         {content}
      </div>
   </Collapsible>;

   const makeUserDataFilters = (d: Translatables): JSX.Element => <div className='userFilters'>
      <div className='field input shortText flex all-parent-row center'>
         <label htmlFor='userName'>
            {d.forms.join.fields.username}
         </label>
         <input
            ref={register}
            placeholder={d.forms.join.fields.usernameDefault}
            name='userName'
            id='userName'
            type='text'
         />
      </div>

      <div className='field input shortText flex all-parent-row center'>
         <label htmlFor='emailAddress'>
            {d.forms.join.fields.emailAddress}
         </label>
         <input
            ref={register}
            placeholder={d.forms.join.fields.emailAddressDefault}
            name='emailAddress'
            id='emailAddress'
            type='email'
         />
      </div>

      <div className='field input shortText flex all-parent-row center'>
         <label htmlFor='givenName'>
            {d.forms.join.fields.givenName}
         </label>
         <input
            ref={register}
            placeholder={d.forms.join.fields.givenNameDefault}
            name='givenName'
            id='givenName'
            type='text'
         />
      </div>

      <div className='field input shortText flex all-parent-row center'>
         <label htmlFor='surname'>
            {d.forms.join.fields.surName}
         </label>
         <input
            ref={register}
            placeholder={d.forms.join.fields.surNameDefault}
            name='surname'
            id='surname'
            type='text'
         />
      </div>

      <div className='field selectList flex all-parent-row center'>
         <label htmlFor='emailVerified'>
            {d.forms.dashboards.emailVerificationStatus}
         </label>
         <Select
            options={emailOptions}
            isMulti={false}
            isSearchable={false}
            name="emailVerified"
            id='emailVerified'
            ref={register}
            value={values.selectedEmailOption}
            onChange={handleEmailChange}
         />
      </div>

      <div className='field input shortText flex all-parent-row center'>
         <label htmlFor='organization'>
            {d.forms.join.fields.organization}
         </label>
         <input
            ref={register}
            placeholder={d.forms.join.fields.organizationDefault}
            name='organization'
            id='organization'
            type='text'
         />
      </div>

      <div className='field selectList flex all-parent-row center'>
         <label htmlFor='jobFocus'>
            {d.forms.join.fields.jobFocus}
         </label>
         <Select
            options={jobOptions}
            isMulti={true}
            isSearchable={true}
            name="jobFocus"
            id='jobFocus'
            ref={register}
            value={values.selectedJobOption}
            onChange={handleMultiJobChange}
         />
      </div>

      <div className='field input shortText flex all-parent-row center'>
         <label htmlFor='orcid'>
            {d.forms.join.fields.orcid}
         </label>
         <input
            ref={register}
            placeholder={d.forms.join.fields.orcidDefault}
            name='orcid'
            id='orcid'
            type='text'
         />
      </div>

      <div className='field selectList flex all-parent-row center'>
         <label htmlFor='country'>
            {d.forms.join.fields.country}
         </label>
         <Select
            options={countryOptions}
            isMulti={false}
            isSearchable={true}
            name="country"
            id='country'
            ref={register}
            value={values.selectedCountryOption}
            onChange={handleCountryChange}
         />
      </div>

      <div className='field selectList flex all-parent-row center'>
         <label htmlFor='newsletter'>
            {d.forms.join.fields.newsletter}
         </label>
         <Select
            options={newsletterOptions}
            isMulti={false}
            isSearchable={false}
            name="newsletter"
            id='newsletter'
            ref={register}
            value={values.selectedNewsletterOption}
            onChange={handleNewseltterChange}
         />
      </div>
   </div>;

   const makeWorkDataFilters = (d: Translatables): JSX.Element => <div className='workFilters'>
      <div className='field input shortText flex all-parent-row center'>
         <label htmlFor='workID'>
         {d.forms.dashboards.workID}
         </label>
         <input
            ref={register}
            placeholder='42'
            name='workID'
            id='workID'
            type='text'
         />
      </div>

      <div className='field selectList flex all-parent-row center'>
         <label htmlFor='octoWorkType'>
            {d.forms.moderate.octoWorkType.fieldTitle}
         </label>
         <Select
            options={octoWorkTypeOptions}
            isMulti={false}
            isSearchable={false}
            name="octoWorkType"
            id='octoWorkType'
            ref={register}
            value={values.selectedOctoWorkTypeOption}
            onChange={handleOctoWorkTypeChange}
         />
      </div>

      <div className='field selectList flex all-parent-row center'>
         <label htmlFor='moderationStatus'>
            {d.forms.dashboards.moderationStatus}
         </label>
         <Select
            options={moderationStatusOptions}
            isMulti={false}
            isSearchable={false}
            name="moderationStatus"
            id='moderationStatus'
            ref={register}
            value={values.selectedModerationStatusOption}
            onChange={handleModerationStatusChange}
         />
      </div>

      <div className='field selectList flex all-parent-row center'>
         <label htmlFor='postedContentType'>
            {d.forms.dashboards.postedContentType}
         </label>
         <Select
            options={postedContentTypeOptions}
            isMulti={false}
            isSearchable={false}
            name="postedContentType"
            id='postedContentType'
            ref={register}
            value={values.selectedPostedContentTypeOption}
            onChange={handlePostedContentTypeChange}
         />
      </div>

      <div className='field input shortText flex all-parent-row center'>
         <label htmlFor='title'>
            {d.forms.submit.basic.title.fieldTitle}
         </label>
         <input
            ref={register}
            placeholder={d.forms.submit.basic.title.default}
            name='title'
            id='title'
            type='text'
         />
      </div>

      <div className='field selectList flex all-parent-row center'>
         <label htmlFor='language'>
            {d.forms.submit.basic.optional.language.fieldTitle}
         </label>
         <Select
            options={languageOptions}
            isMulti={false}
            isSearchable={true}
            name="language"
            id='language'
            ref={register}
            value={values.selectedLanguageOption}
            onChange={handleLanguageChange}
         />
      </div>

      <div className='field selectList flex all-parent-row center'>
         <label htmlFor='peerReviewed'>
            {d.forms.submit.basic.peerReview.fieldTitle}
         </label>
         <Select
            options={peerReviewedOptions}
            isMulti={false}
            isSearchable={false}
            name="peerReviewed"
            id='peerReviewed'
            ref={register}
            value={values.selectedPeerReviewOption}
            onChange={handlePeerReviewChange}
         />
      </div>

      <div className='field input shortText flex all-parent-row center'>
         <label htmlFor='publisherDOI'>
            {d.forms.submit.basic.publisherDOI.fieldTitle}
         </label>
         <input
            ref={register}
            placeholder={d.forms.submit.basic.publisherDOI.default}
            name='publisherDOI'
            id='publisherDOI'
            type='text'
         />
      </div>

      <div className='field input shortText flex all-parent-row center'>
         <label htmlFor='marxivDOI'>
            {d.forms.dashboards.marxivDOI}
         </label>
         <input
            ref={register}
            placeholder={d.forms.submit.basic.publisherDOI.default}
            name='marxivDOI'
            id='marxivDOI'
            type='text'
         />
      </div>

      <div className='field selectList flex all-parent-row center'>
         <label htmlFor='license'>
            {d.forms.submit.license.formTitle}
         </label>
         <Select
            options={licenseSelectOptions}
            isMulti={false}
            isSearchable={false}
            name="license"
            id='license'
            ref={register}
            value={values.selectedLicenseOption}
            onChange={handleLicenseChange}
         />
      </div>

      <div className='field input shortText flex all-parent-row center'>
         <label htmlFor='copyrightHolder'>
            {d.forms.submit.license.copyrightHolder.fieldTitle}
         </label>
         <input
            ref={register}
            placeholder={d.forms.submit.license.copyrightHolder.default}
            name='copyrightHolder'
            id='copyrightHolder'
            type='text'
         />
      </div>

      <div className='field input shortText flex all-parent-row center'>
         <label htmlFor='contributorSurname'>
            {d.forms.dashboards.hasContributorSurname}
         </label>
         <input
            ref={register}
            placeholder={d.forms.join.fields.surNameDefault}
            name='contributorSurname'
            id='contributorSurname'
            type='text'
         />
      </div>

      <div className='field input shortText flex all-parent-row center'>
         <label htmlFor='submittedBy'>
            {d.forms.dashboards.submittedBy}
         </label>
         <input
            ref={register}
            placeholder={d.forms.join.fields.givenNameDefault + ' ' + d.forms.join.fields.surName}
            name='submittedBy'
            id='submittedBy'
            type='text'
         />
      </div>
   </div>;

   const makeAdminDashboardMenu = (d: Translatables): JSX.Element => <div className='filters'>
      <div id='userDataFilter'>
         {makeCollapsibleFilter('userData')(d.forms.dashboards.userFilters)(makeUserDataFilters(d))}
      </div>
      <div id='WorkDataFilter'>
         {makeCollapsibleFilter('workData')(d.forms.dashboards.workFilters)(makeWorkDataFilters(d))}
      </div>
   </div>;

   /**
    * Display user data
    */
   const jobsToString = (jobs: string[] | undefined): string => {
      if (jobs) {
         const jobStrings: string[] = new Array;
         jobs.map((job) => jobStrings.push( translateJobOption(d.forms.join.jobOptions)(job as JobOptionValues) ))
         return jobStrings.join(', ');
      }
      else {
         return ''
      }
   }

   const displayUndefined = (value: any | undefined) => (valueString: string) => (notSetValue: string): string => {
      if (value) {
         return valueString
      }
      else {
         return notSetValue
      }
   }

   const makeUserDataRow = (requestedLanguage: SupportedLanguage) => (user: GenericAuthenticatedUserData) => (key: number): JSX.Element => <div className='userData verticalBuffer1' key={key}>
      <h6>
         <a href={'/' + requestedLanguage + '/adminEditAccount?u=' + user.userName} target='_blank'> {user.userName}</a>
      </h6>
      <div>{user.givenName + ' ' + user.surname + " \u2014 " + user.userType + ';'}</div>
      <div>{user.emailAddress + " \u2014 " + displayUndefined(user.emailVerified)(d.commonReplacements.yes)(d.commonReplacements.no) + ';'}</div>
      <div>{displayUndefined(user.organization)(user.organization as string)(d.commonReplacements.notSet) + " \u2014 " +  translateCountrySelectOption(countrySelectOpts)(user.country ? user.country : '') + ';'}</div>
      <div>{d.forms.join.fields.jobFocus + ': ' + displayUndefined(user.jobFocus)(jobsToString(user.jobFocus))(d.commonReplacements.notSet) + ';'}</div>
      <div>{d.forms.join.fields.orcid + ': ' + displayUndefined(user.orcid)(user.orcid as string)(d.commonReplacements.notSet) + ';'}</div>
      <div>{d.forms.dashboards.newsletter + ': ' + displayUndefined(user.newsletter)(d.commonReplacements.yes)(d.commonReplacements.no) + ';'}</div>
   </div>;

   const combineUserData = (requestedLanguage: SupportedLanguage) => (users: GenericAuthenticatedUserData[]): JSX.Element[] => {
      const elements: JSX.Element[] = new Array;
      users.map( (user: GenericAuthenticatedUserData, i: number) => elements.push( makeUserDataRow(requestedLanguage)(user)(i) ) );
      return elements;
   }

   /**
    * Switch the display type
    */
   const handleDisplaySwitch = (type: DisplayType) => (): void => {
      setReactSelectValue({ ...values, displayType: type });
   }

   /**
    * Remove & Reset filters
    */
   const handleReset = (): void => {
      reset(defaultValues);
      setReactSelectValue(reactSelectInitialValues);
   };

   /**
    * Render the content
    */
   return (<div>
      <h1>
         {d.pageMetadata.admin.title}
      </h1>

      <form id='form' className='verticalBuffer2 flex-all-parent-column' onSubmit={onSubmit}>
         <div className='flex all-child-span100 center' id='adminDashboardSelection'>
            <button type='button' className='button lightBlue' onClick={handleDisplaySwitch('works')}>
               {d.forms.dashboards.displayWorks}
            </button>
            <button type='button' className='button lightBlue' onClick={handleDisplaySwitch('users')}>
               {d.forms.dashboards.displayUsers}
            </button>
         </div>

         <div className='flex all-child-span100' id='adminDashboardMenu'>
            {makeAdminDashboardMenu(d)}
         </div>

         <div id='adminButtons' className='flex all-child-span1 center'>
            <button type='submit' className='button submit'>
               {d.forms.dashboards.applyFilters}
            </button>
            <button type='button' className='button cancel' onClick={handleReset}>
               {d.forms.dashboards.removeFilters}
            </button>
         </div>
      </form>

      <div id='adminDashboardContent'>
         {values.displayType === 'works' ? combineAdminWorkTeasers(requestedLanguage)(d)(works) : null}
         {values.displayType === 'users' ? combineUserData(requestedLanguage)(users) : null}
      </div>

      <div className='flex all-parent-column'>
         <div className='flex all-child-span1 center'>
         {values.displayType === 'works' ? (
            <button type='button' className='button body' onClick={() => console.log('Load more works.')}>
               {d.forms.dashboards.loadMoreWorks}
            </button>
         ) : null}

         {values.displayType === 'users' ? (
            <button type='button' className='button body' onClick={() => console.log('Load more users.')}>
               {d.forms.dashboards.loadMoreUsers}
            </button>
         ) : null}
         </div>
      </div>
   </div>);
};

/**
 * Add the react elements to the DOM
 */
const testingEnglisAdminDashboard = <HookAdministration
   submitBackend={(data: AdministrationFilters) => console.log(data)}
   requestedLanguage={testingContextWithoutTranslatables.requestedLanguage}
   d={translatablesEnglish.translatables}
   works={dummyWorksForListing}
   users={[dummyUserData]}
/>;

const domContainer = document.getElementById('reactAdminDashboard');
if (domContainer) {
   ReactDOM.render(testingEnglisAdminDashboard, domContainer);
};