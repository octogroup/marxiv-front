import * as React from 'react';
import * as ReactDOM from "react-dom";
import { Translatables, translatablesEnglish, makeJobOptions, makeCountrySelectOptions, SelectOption, englishMarXivMinimalSitemap } from '@octogroup/marxiv-common';
import { MarXivSitemapMinimum } from '@octogroup/marxiv-common/dist/sitemap';
import useForm from 'react-hook-form'
import Select from 'react-select';
import { useOcto } from '../helpers/graphqlHook'
import gql from 'graphql-tag';

import { validateEmailAddress, validateORCID, cleanHTML, } from '../helpers/validations';
import { FormErrorMessage, hastToReact } from '../helpers/formHelpers';
import { addEventListeners } from '../scripts/eventListeners';
import { reCAPTCHAv3SiteKey, InvokeReCAPTCHAOptions, rcptaBackoff, } from '../scripts/recaptchaSetup';

interface UserRegistrationMutation {
   username: string
   email: string
   emailConfirm: string
   password: string
   passwordConfirm: string
   given?: string
   surname?: string
   org?: string
   job?: string[]
   orcid?: string
   country?: string
   newsletter?: string
}

const REGISTER_USER = gql`
  mutation RegisterUser($userReg: UserRegister) {
    registerUser (userReg: $userReg) {
       id   }
  }`

export const HookUser = function ({ submitBackend, d, siteMap }: { submitBackend: (u: UserRegistrationMutation) => void, d: Translatables, siteMap: MarXivSitemapMinimum }) {
   const methods = useForm<UserRegistrationMutation>({
      mode: 'onBlur',
      reValidateMode: 'onChange'
   });

   /**
    * Redirect the user to the confirmation page.
    */
   const redirect = (): string => {
      const path: string = window.location.pathname; // "/en/submit/preprint/"
      const lang: string = path.slice(0, 3); // "/en"
      const redirectPath: string = lang + siteMap.confirm.newAccount.localURL;
      return window.location.pathname = redirectPath;
   }

   const initialSelectOption: SelectOption = { label: '', value: '' }
   const register = methods.register
   const errors = methods.errors;
   const onSubmit = methods.handleSubmit(submitBackend);
   const errorMsgs = d.forms.messages.errors;
   const jobOptions = makeJobOptions(d);
   const countryOptions = makeCountrySelectOptions();
   const [values, setReactSelectValue] = React.useState({
      selectedJobOption: [] as Array<SelectOption>,
      selectedCountryOption: initialSelectOption,
   });
   const handleMultiJobChange = (selectedJobOption: SelectOption[]) => {
      let newJobs = [...new Set([...values.selectedJobOption, ...selectedJobOption])];
      setReactSelectValue({ ...values, selectedJobOption: newJobs })
      methods.setValue('job', newJobs.map(x => x.value))
   };
   const handleCountryChange = (selectedCountryOption: SelectOption) => {
      methods.setValue('country', selectedCountryOption.value);
      setReactSelectValue({...values, selectedCountryOption: selectedCountryOption })
   };
   const comparePasswords = () => {
      let current = methods.getValues()
      return current.password === current.passwordConfirm
   };
   const compareEmail = () => {
      if (errors.email || errors.emailConfirm) {
         return
      }
      else {
         let current = methods.getValues()
         return current.email === current.emailConfirm
      }
   };

   // Register the Job and Country fields on each load
   React.useEffect(() => {
      register({ name: 'job'});
      register({ name: 'country'});
   }, [register]);

   // Add event listeners just on load
   React.useEffect(() => {
      return addEventListeners();
   }, []);

   /**
    * We need to wait until after the reCAPTCHA script has loaded.
    * Once it has, `grecaptcha` will be defined
    */
   const onLoadJoin: InvokeReCAPTCHAOptions = {
      sitekey: reCAPTCHAv3SiteKey,
      action: 'register'
   };

   return <div id='form'>
      <div className='form wrapper' id='register'>
         <div className='title flex all-child-span1'>
            {d.forms.join.formTitle}
         </div>
         <div className='content'>
            <form onSubmit={onSubmit}>
               <div className='field input shortText flex all-parent-row center'>
                  <label htmlFor='username'>
                     {d.forms.join.fields.username}
                  </label>
                  <input
                     name="username"
                     ref={methods.register({ required: true, validate: validateEmailAddress })}
                     placeholder={d.forms.join.fields.usernameDefault}
                     type='text'
                     id='username'
                     className={errors.username ? 'invalid' : 'required'}
                  />
               </div>
               {errors.username ? (<FormErrorMessage>{errorMsgs.usernameOrEmail}</FormErrorMessage>) : null}
               <div className='field input email flex all-parent-row center'>
                  <label htmlFor='email'>
                     {d.forms.join.fields.emailAddress}
                  </label>
                  <input
                     ref={methods.register({ required: true, validate: validateEmailAddress })}
                     placeholder={d.forms.join.fields.emailAddressDefault}
                     name="email"
                     type='email'
                     id='email'
                     className={errors.email ? 'invalid' : 'required'}
                  />
               </div>
               {errors.email ? (<FormErrorMessage>{errorMsgs.usernameOrEmail}</FormErrorMessage>) : null}
               <div className='field input email flex all-parent-row center'>
                  <label htmlFor='emailConfirm'>
                     {d.forms.join.fields.confirmEmailAddress}
                  </label>
                  <input
                     ref={methods.register({ required: true, validate: validateEmailAddress })}
                     placeholder={d.forms.join.fields.emailAddressDefault}
                     name="emailConfirm"
                     type='email'
                     id='emailConfirm'
                     className={errors.emailConfirm ? 'invalid' : 'required'}
                  />
               </div>
               {errors.emailConfirm ? (<FormErrorMessage>{errorMsgs.usernameOrEmail}</FormErrorMessage>) : null}
               {compareEmail() ? null : (<FormErrorMessage>{errorMsgs.emailAddressNoMatch}</FormErrorMessage>)}
               <div className='field input password flex all-parent-row center'>
                  <label htmlFor='password'>
                     {d.forms.join.fields.password}
                  </label>
                  <input
                     ref={methods.register({
                        required: d.forms.messages.errors.passwordInvalid,
                        minLength: {
                           value: 8,
                           message: d.forms.messages.errors.passwordShort
                        },
                        maxLength: {
                           value: 128,
                           message: d.forms.messages.errors.passwordLong
                        },
                        validate: cleanHTML(d.forms.messages.errors.passwordInvalid)
                     })}
                     placeholder={d.forms.join.fields.passwordDefault}
                     name="password"
                     type='password'
                     id='password'
                     className={errors.password ? 'invalid' : 'required'}
                  />
               </div>
               {errors.password && errors.password.message ? (<FormErrorMessage>{errors.password.message}</FormErrorMessage>) : null}
               <div className='field input password flex all-parent-row center'>
                  <label htmlFor='passwordConfirm'>
                     {d.forms.join.fields.confirmPassword}
                  </label>
                  <input
                     ref={methods.register({
                        required: d.forms.messages.errors.passwordInvalid,
                        minLength: {
                           value: 8,
                           message: d.forms.messages.errors.passwordShort
                        },
                        maxLength: {
                           value: 128,
                           message: d.forms.messages.errors.passwordLong
                        },
                        validate: cleanHTML(d.forms.messages.errors.passwordInvalid)
                     })}
                     placeholder={d.forms.join.fields.passwordDefault}
                     name="passwordConfirm"
                     type='password'
                     id='passwordConfirm'
                     className={errors.passwordConfirm ? 'invalid' : 'required'}
                  />
               </div>
               {errors.passwordConfirm && errors.passwordConfirm.message ? (<FormErrorMessage>{errors.passwordConfirm.message}</FormErrorMessage>) : null}
               {comparePasswords() ? null : (<FormErrorMessage>{errorMsgs.passwordNoMatch}</FormErrorMessage>)}
               <div className='spacer verticalBuffer2' />
               <div className='field input shortText flex all-parent-row center'>
                  <label htmlFor='given'>
                     {d.forms.join.fields.givenName}
                  </label>
                  <input
                     ref={methods.register({
                        required: d.forms.messages.errors.givenName,
                        validate: cleanHTML(d.forms.messages.errors.givenName)
                     })}
                     placeholder={d.forms.join.fields.givenNameDefault}
                     name='given'
                     id='given'
                     type='text'
                     className={errors.given ? 'invalid' : 'required'}
                  />
               </div>
               {errors.given && errors.given.message ? (<FormErrorMessage>{errors.given.message}</FormErrorMessage>) : null}
               <div className='field input shortText flex all-parent-row center'>
                  <label htmlFor='surname'>
                     {d.forms.join.fields.surName}
                  </label>
                  <input
                     ref={methods.register({
                        required: d.forms.messages.errors.surname,
                        validate: cleanHTML(d.forms.messages.errors.surname)
                     })}
                     placeholder={d.forms.join.fields.surNameDefault}
                     name='surname'
                     id='surname'
                     type='text'
                     className={errors.surname ? 'invalid' : 'required'}
                  />
               </div>
               {errors.surname && errors.surname.message ? (<FormErrorMessage>{errors.surname.message}</FormErrorMessage>) : null}
               <div className='field input shortText flex all-parent-row center'>
                  <label htmlFor='org'>
                     {d.forms.join.fields.organization}
                  </label>
                  <input
                     ref={methods.register({
                        validate: cleanHTML(d.forms.messages.errors.generalInvlidShortText)
                     })}
                     placeholder={d.forms.join.fields.organizationDefault}
                     name='org'
                     id='org'
                     type='text'
                     className={errors.org ? 'invalid' : undefined}
                  />
               </div>
               {errors.org && errors.org.message ? (<FormErrorMessage>{errors.org.message}</FormErrorMessage>) : null}
               <div className='field selectListMulti flex all-parent-row center'>
                  <label htmlFor='job'>
                     {d.forms.join.fields.jobFocus}
                  </label>
                  <Select
                     options={jobOptions}
                     isMulti={true}
                     isSearchable={true}
                     id='job'
                     name="job"
                     value={values.selectedJobOption}
                     onChange={handleMultiJobChange}
                  />
               </div>
               <div className='field input shortText flex all-parent-row center'>
                  <label htmlFor='orcid'>
                     {d.forms.join.fields.orcid}
                  </label>
                  <input
                     ref={methods.register({
                        validate: validateORCID(d)
                     })}
                     placeholder={d.forms.join.fields.orcidDefault}
                     name='orcid'
                     id='orcid'
                     type='text'
                  />
               </div>
               <div className='field selectList flex all-parent-row center'>
                  <label htmlFor='country'>
                     {d.forms.join.fields.country}
                  </label>
                  <Select
                     options={countryOptions}
                     isMulti={false}
                     isSearchable={true}
                     id='country'
                     name="country"
                     ref={register}
                     value={values.selectedCountryOption}
                     onChange={handleCountryChange}
                  />
               </div>
               <div className='spacer verticalBuffer2' />
               <div className='newsletter flex all-parent-row'>
                  <div className='field inputTitle flex all-child-span2'>
                     {d.forms.join.fields.newsletter}
                  </div>
                  <div className='radioGroup flex all-parent-row flex all-child-span1 center'>
                     <div className='radio'>
                        <input
                           type='radio'
                           id='newsletterYes'
                           value='newsletterYes'
                           name='newsletter'
                           ref={methods.register({
                              required: d.forms.messages.errors.newsletter
                           })}
                        />
                        <label htmlFor='newsletterYes'>
                           {d.commonReplacements.yes}
                        </label>
                     </div>
                     <div className='radio'>
                        <input
                           type='radio'
                           id='newsletterNo'
                           value='newsletterNo'
                           name='newsletter'
                           ref={methods.register({
                              required: d.forms.messages.errors.newsletter
                           })}
                        />
                        <label htmlFor='newsletterNo'>
                           {d.commonReplacements.no}
                        </label>
                     </div>
                  </div>
               </div>
               <div className='form end flex all-parent-row'>
                  <div className='flex all-child-span1 center'>
                     <button type="reset" className='button cancel'>
                        {d.commonReplacements.cancel}
                     </button>
                  </div>
                  <div className='flex all-child-span1 center'>
                     <button type="submit" className='button submit'>
                        {d.commonReplacements.submit}
                     </button>
                  </div>
               </div>
               <div className='flex all-parent-row'>
                  <div className='verticalBuffer1' dangerouslySetInnerHTML={hastToReact(d.forms.grecaptcha.terms)} />
               </div>
            </form>
         </div>
      </div>

      {() => rcptaBackoff(onLoadJoin)}

   </div>
}

const englishUserRegistrationForm =
   <HookUser
      submitBackend={(data: any) => console.log(data)}
      d={translatablesEnglish.translatables}
      siteMap={englishMarXivMinimalSitemap}
   />;

const domContainer = document.getElementById('reactUserRegister');
if (domContainer) {
   ReactDOM.render(englishUserRegistrationForm, domContainer);
}
