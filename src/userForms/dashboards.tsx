/**
 * User and Moderation Dashboards
 * 
 * __We need to add pagination__
 */
import * as React from 'react';
import * as ReactDOM from "react-dom";
import { Translatables, translatablesEnglish, testingContextWithoutTranslatables, CommonReplacements, ActiveWorkData, translateModerationStatus, SupportedLanguage, } from '@octogroup/marxiv-common';
import { UserStatus, } from '@octogroup/marxiv-common/dist/context';
import { StaticWorkText } from '@octogroup/marxiv-common/dist/translatables';
import { dummyWorksForListing, } from '@octogroup/marxiv-common/dist/testingData';
import { translateWorkTypeBadge, formatOriginalPublicationDate, translateLicenseBadge, OctoWorkTypeAliases } from '@octogroup/marxiv-common/dist/works';

import { reCAPTCHAv3SiteKey, InvokeReCAPTCHAOptions, rcptaBackoff, } from '../scripts/recaptchaSetup';
import { addEventListeners } from '../scripts/eventListeners';

const HookUserDashboard = function ({ userType, requestedLanguage, d, works }: { userType: UserStatus, requestedLanguage: SupportedLanguage, d: Translatables, works: ActiveWorkData[] }) {
   // Add event listeners just on load
   React.useEffect(() => {
      // Add event listeners just on load
      return addEventListeners();
   }, []);

   /**
    * We need to wait until after the reCAPTCHA script has loaded.
    * Once it has, `grecaptcha` will be defined
    */
   const onLoadDashboard: InvokeReCAPTCHAOptions = {
      sitekey: reCAPTCHAv3SiteKey,
      action: 'form'
   };

   /**
    * Make the components.
    */
   const makeModerationBadge = (d: StaticWorkText) => (work: ActiveWorkData): JSX.Element => {
      if (work.moderationStatus === 'rejected' || work.moderationStatus === 'withdrawn') {
         return <div className='badge red center'>
            {translateModerationStatus(d)(work)}
         </div>
      }
      else if (work.moderationStatus === 'pending') {
         return <div className='badge orange center'>
            {translateModerationStatus(d)(work)}
         </div>
      }
      else if (work.marxivDOI) {
         return <div className='badge grey center'>
            {d.doi + ': ' + work.marxivDOI}
         </div>
      }
      else {
         return <div className='badge grey center'>
            {translateModerationStatus(d)(work)}
         </div>
      }
   };

   const makeTypeBadge = (d: StaticWorkText) => (work: ActiveWorkData): JSX.Element => <div className='badge orange center'>
      {translateWorkTypeBadge(d)(work)}
   </div>;

   const makeDateBadge = (d: CommonReplacements) => (work: ActiveWorkData): JSX.Element => {
      if (work.originalPublicationDate) {
         return <div className='badge blue center'>
            {formatOriginalPublicationDate(d)(work.originalPublicationDate)}
         </div>
      }
      else {
         return <div className='badge blue center'>
            {d.notSet}
         </div>
      }
   };

   const makeLicenseBadge = (d: StaticWorkText) => (work: ActiveWorkData): JSX.Element => <div className='badge body center'>
      {translateLicenseBadge(d)(work)}
   </div>;

   const makeEditLink = (requestedLanguage: SupportedLanguage) => (work: ActiveWorkData): string => {
      const editFormType = (workType: OctoWorkTypeAliases | undefined): string => {
         if (workType) {
            switch (workType) {
               case 'letter':
                  return 'letter';
               case 'oaPaper':
                  return 'oa';
               case 'poster':
                  return 'poster';
               case 'postprint':
                  return 'postprint';
               case 'preprint':
                  return 'preprint';
               case 'reportUnpublished':
                  return 'reportUnpublished';
               case 'thesis':
                  return 'thesis';
               case 'copyrightedPaper':
                  return 'uncopyrighted';
               case 'workingPaper':
                  return 'workingPaper';
               default:
                  return 'preprint';
            }
         }
         else {
            return 'preprint'
         }
      };
      const base: string = '/' + requestedLanguage + '/edit/';
      const type: string = editFormType(work.octoWorkType);
      const id: string = '?id=' + work.workID;
      return base + type + id;
   }

   const makeEditButton = (requestedLanguage: SupportedLanguage) => (d: CommonReplacements) => (work: ActiveWorkData): JSX.Element => <a className='button orange' href={makeEditLink(requestedLanguage)(work)} >
      {d.edit}
   </a>;

   /**
    * Make dashboards for regular users
    */
   const makeUserWorkTeaser = (requestedLanguage: SupportedLanguage) => (d: Translatables) => (work: ActiveWorkData) => (i: number): JSX.Element => <div key={i} className='paper teaser'>
      <h2>
         <a href={'/' + requestedLanguage + '/works/' + work.workID}>
            {work.title ? work.title : d.commonReplacements.notSet}
         </a>
      </h2>
      <div className='metadataBar flex all-parent-row'>
         {makeDateBadge(d.commonReplacements)(work)}
         {makeTypeBadge(d.works.static)(work)}
         {makeLicenseBadge(d.works.static)(work)}
         {makeModerationBadge(d.works.static)(work)}
      </div>
      <p className='verticalBuffer2'>
         {work.description ? work.description : d.commonReplacements.notSet}
      </p>
      <p>
         {makeEditButton(requestedLanguage)(d.commonReplacements)(work)}
      </p>
   </div>;

   const combineUserWorkTeasers = (requestedLanguage: SupportedLanguage) => (d: Translatables) => (works: ActiveWorkData[]): JSX.Element[] => {
      let teasers: JSX.Element[] = new Array;
      for (let i = 0; i < works.length; i++) {
         teasers.push(makeUserWorkTeaser(requestedLanguage)(d)(works[i])(i))
      }
      return teasers
   };

   const makeUserDashboard = (requestedLanguage: SupportedLanguage) => (d: Translatables) => (works: ActiveWorkData[]): JSX.Element => <div>
      <h1>
         {d.works.static.userDashboardTitle}
      </h1>
      <div id='userDashboardContent'>
         {combineUserWorkTeasers(requestedLanguage)(d)(works)}
      </div>
   </div>;

   /**
    * Determine the user type, and export the appropriate dashboard
    */
   const makeDashboard = (userType: UserStatus) => (requestedLanguage: SupportedLanguage) => (d: Translatables) => (works: ActiveWorkData[]): JSX.Element => {
      if (userType === 'user' || userType === 'org') {
         return makeUserDashboard(requestedLanguage)(d)(works)
      }
      else {
         // We should give the user a 501 Error Page, ideally
         return <div>
            {d.commonReplacements.notSet}
         </div>
      }
   }

   /**
    * Render the content
    */
   return (<div>
      {makeDashboard(userType)(requestedLanguage)(d)(works)}
      {() => rcptaBackoff(onLoadDashboard)}
   </div>)
};

/**
 * Add the react elements to the DOM
 */
const testingEnglisDashboard = <HookUserDashboard
   userType={'user'}
   requestedLanguage={testingContextWithoutTranslatables.requestedLanguage}
   d={translatablesEnglish.translatables}
   works={dummyWorksForListing}
/>;

const domContainer = document.getElementById('reactDashboard');
if (domContainer) {
   ReactDOM.render(testingEnglisDashboard, domContainer);
};