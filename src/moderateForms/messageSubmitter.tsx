import * as React from 'react';
import { Translatables,  } from '@octogroup/marxiv-common';
import { useFormContext } from 'react-hook-form';

import { required } from '../helpers/validations';
import { FormErrorMessage, hastToReact } from '../helpers/formHelpers';

/**
 * Message to the Submitter
 */
export interface SubmissionMessageState {
   messageToSubmitter: string
}

export const HookMessageToSubmitter = function ({ d, initialValues }: { d: Translatables, initialValues: SubmissionMessageState }) {
   // Get the parent form data
   const parentMethods = useFormContext();
   const parentRegister = parentMethods.register;
   const parentErrors = parentMethods.errors;

   /**
    * Render the content
    */
   return <div className='form wrapper'>
      <div className='title flex all-child-span1'>
         {d.forms.moderate.messageToSubmitter.formTitle}
      </div>
      <div className='content'>

         <div className='field'>
            <div className='description' dangerouslySetInnerHTML={hastToReact(d.forms.moderate.messageToSubmitter.description)} />
         </div>

         <div className='field input longText flex all-parent-column'>
            <label className='flex all-child-span1' htmlFor='messageToSubmitter'>
               {d.forms.moderate.messageToSubmitter.message.fieldTitle}
            </label>
            <textarea
               id='messageToSubmitter'
               name='messageToSubmitter'
               className={parentErrors.messageToSubmitter && parentErrors.messageToSubmitter.message ? 'flex all-child-span100 invalid' : 'flex all-child-span100 required'}
               placeholder={d.forms.moderate.messageToSubmitter.message.default}
               ref={parentRegister({
                  validate: required(d.forms.messages.errors.generalRequired),
               })}
            />
         </div>
         {parentErrors.messageToSubmitter && parentErrors.messageToSubmitter.message ? (<FormErrorMessage>{parentErrors.messageToSubmitter.message}</FormErrorMessage>) : null}

      </div>
   </div>

}

