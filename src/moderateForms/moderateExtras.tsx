import * as React from 'react';
import { Translatables, SelectOption, makePostedContentTypeOptions, PostedContentType, makeBadgeTypeOptions, WorkTypeBadge } from '@octogroup/marxiv-common';
import { useFormContext } from 'react-hook-form';
import Select from 'react-select';

import { required } from '../helpers/validations';
import { FormErrorMessage, getDefaultValue } from '../helpers/formHelpers';

/**
 * Moderation fields for MarXiv admins
 */
export interface ModerationExtrasState {
   postedContentType: PostedContentType | ''
   badgeType: WorkTypeBadge | ''
   vorCost: string
}

export const HookModerationExtras = function ({ d, initialValues }: { d: Translatables, initialValues: ModerationExtrasState }) {
   // Get the parent form data
   const parentMethods = useFormContext();
   const parentRegister = parentMethods.register;
   const parentErrors = parentMethods.errors;
   const setParentValue = parentMethods.setValue;

   // Register values for React-state controlled data
   React.useEffect(() => {
      parentRegister({ name: 'postedContentType' }, { validate: required(d.forms.messages.errors.generalRequired) });
      parentRegister({ name: 'badgeType' }, { validate: required(d.forms.messages.errors.generalRequired) });
   }, [parentRegister]);

   // Set our Select options
   const postedContentTypeSelectOptions: SelectOption[] = makePostedContentTypeOptions(d);
   const badgeTypeSelectOptions: SelectOption[] = makeBadgeTypeOptions(d.works.static);

   // Set our initial values for React-state controlled data
   const [values, setReactSelectValue] = React.useState({
      selectedPostedContentTypeOption: getDefaultValue(initialValues.postedContentType)(postedContentTypeSelectOptions),
      selectedBadgeTypeOption: getDefaultValue(initialValues.badgeType)(badgeTypeSelectOptions),
   });

   const handlePostedContentTypeChange = (selectedPostedContentTypeOption: SelectOption): void => {
      setParentValue('postedContentType', selectedPostedContentTypeOption.value);
      setReactSelectValue({ ...values, selectedPostedContentTypeOption: selectedPostedContentTypeOption })
   };

   const handleBadgeTypeChange = (selectedBadgeTypeOption: SelectOption): void => {
      setParentValue('badgeType', selectedBadgeTypeOption.value);
      setReactSelectValue({ ...values, selectedBadgeTypeOption: selectedBadgeTypeOption })
   };

   /**
    * Render the content
    */
   return <div id='basicInformationPostedContent'>
      <div className='form wrapper'>
         <div className='title flex all-child-span1'>
            {d.forms.submit.basic.formTitle}
         </div>
         <div className='content'>
            
            <div className='field selectList flex all-parent-row center'>
               <label htmlFor='postedContentType'>
                  {d.forms.moderate.octoWorkType.fieldTitle}
               </label>
               <Select
                  options={postedContentTypeSelectOptions}
                  isMulti={false}
                  isSearchable={false}
                  name="postedContentType"
                  id='postedContentType'
                  ref={parentRegister}
                  value={values.selectedPostedContentTypeOption}
                  onChange={handlePostedContentTypeChange}
               />
            </div>
            {parentErrors.postedContentType && parentErrors.postedContentType.message ? (<FormErrorMessage>{parentErrors.postedContentType.message}</FormErrorMessage>) : null}

            <div className='field selectList flex all-parent-row center'>
               <label htmlFor='badgeType'>
                  {d.forms.moderate.octoWorkType.badgeTypeFieldTitle}
               </label>
               <Select
                  options={badgeTypeSelectOptions}
                  isMulti={false}
                  isSearchable={false}
                  name="badgeType"
                  id='badgeType'
                  ref={parentRegister}
                  value={values.selectedBadgeTypeOption}
                  onChange={handleBadgeTypeChange}
               />
            </div>
            {parentErrors.badgeType && parentErrors.badgeType.message ? (<FormErrorMessage>{parentErrors.badgeType.message}</FormErrorMessage>) : null}
           
            <div className='field input shortText flex all-parent-row center'>
               <label htmlFor='vorCost'>
                  {d.forms.moderate.vorCost.fieldTitle}
               </label>
               <input
                  ref={parentRegister}
                  placeholder={d.forms.moderate.vorCost.default}
                  name='vorCost'
                  id='vorCost'
                  type='number'
                  step={0.01}
                  min={0.01}
                  className={parentErrors.vorCost && parentErrors.vorCost.message ? 'invalid' : undefined}
                  onBlur={(event) => parentMethods.setValue('title', event.currentTarget.value)}
               />
            </div>

         </div>
      </div>
   </div>
}

