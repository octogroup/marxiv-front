import * as React from 'react';
import { Translatables } from '@octogroup/marxiv-common';

/**
 * Moderate a PDF file
 */
export interface FileModerateState {
   // File location
   fileHref: string
}

export const HookFileModerate = function ({ d, fileHref }: { d: Translatables, fileHref: string }) {
   /**
    * Setup the PDF file viewer.
    */
   const viewerStarter = '../pdf/web/viewer.html?file=';
   const viewer = viewerStarter + fileHref;

   /**
    * Render the "form"
    */
   return <div id='moderatePDFFile'>
      <div className='form wrapper'>
         <div className='title flex all-child-span1'>
            {d.forms.submit.files.pdf.formTitle}
         </div>
         <div className='content'>
            
            <div id='preview' className='preview flex all-parent-column center'>
               <embed id='embed' type='application/pdf' src={fileHref} />

               <a href={viewer} target='_blank'>
                  {d.works.static.openPDF}
               </a>

               <script src='/../js/hidePDF.js' type='module' defer={true} />
            </div>
         
         </div>
      </div>
   </div>
}