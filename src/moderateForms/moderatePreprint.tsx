import * as React from 'react';
import { render } from "react-dom";
import { Translatables, translatablesEnglish, LicenseOption, englishMarXivMinimalSitemap } from '@octogroup/marxiv-common';
import { MarXivSitemapMinimum } from '@octogroup/marxiv-common/dist/sitemap';
import useForm from 'react-hook-form'


import { Contributor, Relationship, determineModerateFormType, FormType } from '../helpers/formHelpers';
import { HookContribs } from '../submitForms/contributors';
import { HookBasicInfo, PostedContentState } from '../submitForms/basicInfo';
import { HookRelationships } from '../submitForms/relationships';
import { HookLicenseCopyright, LicenseState } from '../submitForms/license';
import { reCAPTCHAv3SiteKey, InvokeReCAPTCHAOptions, } from '../scripts/recaptchaSetup';

import { HookFileModerate, FileModerateState } from './fileModerate';
import { HookModerationExtras, ModerationExtrasState } from './moderateExtras';
import { HookMessageToSubmitter, SubmissionMessageState } from './messageSubmitter';

/**
 * We can extract the Work ID with `window.location.search` 
 * 
 * if `window.location.href` = "http://marxiv.test/en/moderate/preprint/?id=1"
 * then
 * `window.location.search` = "?id=1"
 */

/**
 * Ensure that license and peerReviewed cannot be empty strings
 */
export interface ModerateFormState extends PostedContentState, LicenseState, FileModerateState, ModerationExtrasState, SubmissionMessageState {
   license: LicenseOption
   peerReviewed: 'peerReviewed' | 'notPeerReviewed'
   // Contribs
   contributors: Contributor[]
   // Relationships between works
   relationships: Relationship[]
}

const HookModeratePreprint = function ({ getExistingData, submitBackend, d, siteMap }: { getExistingData: () => ModerateFormState, submitBackend: (u: ModerateFormState) => void, d: Translatables, siteMap: MarXivSitemapMinimum }) {
   // Populate default values from existing data.
   const initialFormState = getExistingData();

   const { handleSubmit, register, unregister, errors, setValue, getValues, watch, } = useForm<ModerateFormState>({
      mode: 'onBlur',
      reValidateMode: 'onBlur',
      defaultValues: initialFormState
   });

   /**
    * Redirect the user to the Admin Dashboard.
    */
   const redirect = (): string => {
      const path: string = window.location.pathname; // "/en/submit/preprint/"
      const lang: string = path.slice(0, 3); // "/en"
      const redirectPath: string = lang + siteMap.admin.localURL;
      return window.location.pathname = redirectPath;
   }

   const onSubmitCB = data => {
      console.log("Submitted parent form with data:");
      console.log(data);

      // Redirect the user to the admin dash
      // redirect();
   }

   /**
    * Determine the form type
    */
   const formType: FormType = determineModerateFormType();

   const isPostprint: boolean = formType === 'postprint' ? true : false;

   const getFormTitle = (formType: FormType): string => {
      switch (formType) {
         case 'preprint':
            return d.forms.moderate.titles.preprint;
         case 'postprint':
            return d.forms.moderate.titles.postprint;
         case 'oa':
            return d.forms.moderate.titles.openAccess;
         case 'uncopyrighted':
            return d.forms.moderate.titles.unCopyrighted;
         case 'reportUnpublished':
            return d.forms.moderate.titles.unpublishedReport;
         case 'thesis':
            return d.forms.moderate.titles.thesis;
         case 'poster':
            return d.forms.moderate.titles.posterPresentationSI;
         case 'letter':
            return d.forms.moderate.titles.letter;
         case 'workingPaper':
            return d.forms.moderate.titles.workingPaper;
         default:
            return d.forms.moderate.titles.preprint;
      }
   }

   /**
    * We need to wait until after the reCAPTCHA script has loaded.
    * Once it has, `grecaptcha` will be defined
    */
   const onLoadForm: InvokeReCAPTCHAOptions = {
      sitekey: reCAPTCHAv3SiteKey,
      action: 'form'
   };

   /**
    * Render the moderation form
    */
   return <div id='form' className='form'>
      <h1>
         {getFormTitle(formType)}
      </h1>

      <div className='verticalBuffer1'>
         <form onSubmit={handleSubmit(onSubmitCB)} id='uploadPreprint'>

            <div className='verticalBuffer1'>
               <HookFileModerate
                  d={translatablesEnglish.translatables}
                  fileHref={initialFormState.fileHref}
               />
            </div>

            <div className='verticalBuffer1'>
               <HookBasicInfo
                  d={translatablesEnglish.translatables}
                  initialValues={{
                     title: initialFormState.title,
                     language: initialFormState.language,
                     peerReviewed: initialFormState.peerReviewed,
                     subtitle: initialFormState.subtitle,
                     publisherDOI: initialFormState.publisherDOI,
                     description: initialFormState.description,
                     originalPublicationYear: initialFormState.originalPublicationYear,
                     originalPublicationMonth: initialFormState.originalPublicationMonth,
                     originalPublicationDay: initialFormState.originalPublicationDay,
                  }}
                  isPostprint={isPostprint}
                  register={register}
                  setValue={setValue}
                  errors={errors}
               />
            </div>

            <div className='verticalBuffer1'>
               <HookModerationExtras
                  d={translatablesEnglish.translatables}
                  initialValues={{
                     postedContentType: initialFormState.postedContentType,
                     badgeType: initialFormState.badgeType,
                     vorCost: initialFormState.vorCost,
                  }}
               />
            </div>

            <div className='verticalBuffer1'>
               <HookContribs
                  d={translatablesEnglish.translatables}
                  register={register}
                  setValue={setValue}
                  getValues={getValues}
               />
            </div>

            <div className='verticalBuffer1'>
               <HookRelationships
                  d={translatablesEnglish.translatables}
                  register={register}
                  setValue={setValue}
                  getValues={getValues}
               />
            </div>

            <div className='verticalBuffer1'>
               <HookLicenseCopyright
                  d={translatablesEnglish.translatables}
                  initialValues={{
                     license: initialFormState.license,
                     copyrightHolder: initialFormState.copyrightHolder,
                  }}
                  isPostprint={isPostprint}
                  register={register}
                  unregister={unregister}
                  watch={watch}
                  errors={errors}
                  setValue={setValue}
               />
            </div>

            <div className='verticalBuffer1'>
               <HookMessageToSubmitter
                  d={translatablesEnglish.translatables}
                  initialValues={{
                     messageToSubmitter: initialFormState.messageToSubmitter
                  }}
               />
            </div>

            <div className='form end flex all-parent-row'>
               <div className='flex all-child-span1 center'>
                  <button type='reset' className='button cancel'>
                     {d.commonReplacements.cancel}
                  </button>
               </div>
               <div className='flex all-child-span1 center'>
                  <button type='submit' className='button submit' onClick={handleSubmit(onSubmitCB)}>
                     {d.commonReplacements.submit}
                  </button>
               </div>
            </div>

         </form>
      </div>

   </div>
}

/**
 * Dummy backend to get data
 */
const getExistingData = (): ModerateFormState => ({
   // File upload
   fileHref: '/../pdf/test.pdf',
   // Basic information
   title: 'This is the title of a work',
   language: 'en',
   peerReviewed: 'notPeerReviewed',
   subtitle: 'This is a subtitle',
   publisherDOI: '10.321/osf.io/testDOI',
   // Original publication date
   originalPublicationYear: '2019',
   originalPublicationMonth: '10',
   originalPublicationDay: '31',
   // License & copyright
   license: 'ccBy',
   copyrightHolder: '',
   // Description
   description: 'Hello, kitty!',
   // Contribututors
   contributors: [{
      surname: 'OCTO',
      nameStyle: 'western',
      role: 'author',
      sequence: 'first',
      order: '1',
      isOrg: 'on',
      id: '125874619844.268434164'
   }],
   // Relationships between works
   relationships: [],
   // Moderation extras
   postedContentType: 'preprint',
   badgeType: '',
   vorCost: '',
   // Message to Submitter
   messageToSubmitter: ''
})

/**
 * Mount to the DOM
 */
const englishModeratePreprintForm =
   <HookModeratePreprint
      getExistingData={getExistingData}
      submitBackend={(data: any) => console.log(data)}
      d={translatablesEnglish.translatables}
      siteMap={englishMarXivMinimalSitemap}
   />;

const domContainer = document.getElementById('reactModerationForm');

if (domContainer) {
   render(englishModeratePreprintForm, domContainer);
}