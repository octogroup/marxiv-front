/**
 * Google Site Tag interfaces
 */
export type GTagMethod = 'js' | 'config' | 'event';

export type GTagAction = 'click' | 'field';

export type GTagEventCategory = 'localLink' | 'outboundLink' | 'onChange' | 'download';

export type GTag = (method: GTagMethod, action: GTagAction, eventInfo: {
   event_category: GTagEventCategory,
   event_label: string,
   value?: number,
   transport_type?: 'beacon',
   event_callback?: () => void
}) => void

export type DataLayer = Array<{ [key:string]: string }>