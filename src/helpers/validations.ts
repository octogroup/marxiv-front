import { Translatables, FormText } from '@octogroup/marxiv-common';
import { Contributor } from './formHelpers'

/**
 * Make custom validations for Formik
 */
export const alphabet: string[] = [
   'a',
   'b',
   'c',
   'd',
   'e',
   'f',
   'g',
   'h',
   'i',
   'j',
   'k',
   'l',
   'm',
   'n',
   'o',
   'p',
   'q',
   'r',
   's',
   't',
   'u',
   'v',
   'w',
   'x',
   'y',
   'z',
   'A',
   'B',
   'C',
   'D',
   'E',
   'F',
   'G',
   'H',
   'I',
   'J',
   'K',
   'L',
   'M',
   'N',
   'O',
   'P',
   'Q',
   'R',
   'S',
   'T',
   'U',
   'V',
   'W',
   'X',
   'Y',
   'Z'
]
export const numbers: string[] = [
   '1',
   '2',
   '3',
   '4',
   '5',
   '6',
   '7',
   '8',
   '9',
   '0'
]
export const numbersWithHyphen: string[] = [
   '1',
   '2',
   '3',
   '4',
   '5',
   '6',
   '7',
   '8',
   '9',
   '0',
   '-'
]

export const alphaNumeric: string[] = [
   ...alphabet,
   ...numbers
]

export const emailChars: string[] = [
   '.',
   '!',
   '#',
   '$',
   '%',
   '&',
   '\'',
   '*',
   '+',
   '-',
   '/',
   '=',
   '?',
   '^',
   '_',
   '`',
   '{',
   '}',
   '|',
   '~',
   '@'
]

export const allowedEmailChars: string[] = [
   ...alphaNumeric,
   ...emailChars
]

export const cleanInput = (value: string) => (allowedCharacters: string[]): string => value.replace(/./g, (c) => {
   if (allowedCharacters.join().includes(c)) {
      return c
   }
   else {
      return ''
   }
});

export const compareCleanedInput = (value: string) => (cleanedInput: string): boolean => {
   if (value === cleanedInput) {
      return true
   }
   else {
      return false
   }
}

export const validateEmailAddress = (value: string): boolean => {
   if (!value || value === '') {
      return false //return d.forms.messages.errors.usernameOrEmail;
   }
   else {
      let cleanedInput = cleanInput(value)(allowedEmailChars);
      let comparedInput = compareCleanedInput(value)(cleanedInput);
      if (comparedInput) {
         return true
      }
      else {
         return false //d.forms.messages.errors.usernameOrEmail;
      }
   }
}

export const validateOptionalEmailAddress = (d: Translatables) => (value: string): string | true  => {
   if (!value || value === '') {
      return true;
   }
   else {
      let cleanedInput = cleanInput(value)(allowedEmailChars);
      let comparedInput = compareCleanedInput(value)(cleanedInput);
      if (comparedInput) {
         return true
      }
      else {
         return d.forms.messages.errors.usernameOrEmail;
      }
   }
}

export const required = (errMsg: string) => (value: string): string | true => {
   if (value && value.length > 0) {
      return true
   }
   else {
      return errMsg;
   }
}

export const validateORCID = (d: Translatables) => (value: string): string | true => {
   const cleanedInput: string = value.replace(/./g, (c) => {
      if (numbersWithHyphen.join().includes(c)) {
         return c
      }
      else {
         return ''
      }
   });
   if (value.length === 0) {
      return true
   }
   else if (value.length > 19 || value.length < 19) {
      return d.forms.messages.errors.orcid;
   }
   else if (cleanedInput === value) {
      return true
   }
   else {
      return d.forms.messages.errors.orcid;
   }
}

export const cleanHTML = (errMsg: string) => (value: string): string | true => {
   // Make a dummy div
   let dummyDiv = document.createElement("div");
   dummyDiv.style.display = 'none';
   dummyDiv.innerHTML = value;
   let cleanedText = dummyDiv.innerText;
   if (value === cleanedText) {
      return true
   }
   else {
      return errMsg;
   }
}

export const validatPassword = (d: Translatables) => (value: string): string | true => {
   if (value.length < 1) {
      return d.forms.messages.errors.passwordInvalid
   }
   else if (value.length < 8) {
      return d.forms.messages.errors.passwordShort
   }
   else if (value.length > 128) {
      return d.forms.messages.errors.passwordLong
   }
   else {
      return cleanHTML(d.forms.messages.errors.passwordInvalid)(value)
   }
}

export const validatOptionalPassword = (d: Translatables) => (value: string): string | true => {
   if (value.length < 1) {
      return true
   }
   else if (value.length < 8) {
      return d.forms.messages.errors.passwordShort
   }
   else if (value.length > 128) {
      return d.forms.messages.errors.passwordLong
   }
   else {
      return cleanHTML(d.forms.messages.errors.passwordInvalid)(value)
   }
}


/**
 * Validate Titles and Subtitles
 */
export const validateTitle = (d: Translatables) => (userInput: string): string | true => {
   // Titles cannot be longer than 255 characters
   if (userInput.length > 255) {
      return d.forms.messages.errors.title
   }
   else if (userInput.length > 1) {
      return cleanHTML(d.forms.messages.errors.title)(userInput)
   }
   else {
      return d.forms.messages.errors.title
   }
}
export const validateSubtitle = (d: Translatables) => (userInput: string): string | true => {
   // Titles cannot be longer than 255 characters
   if (userInput.length > 255) {
      return d.forms.messages.errors.title
   }
   else if (userInput.length > 1) {
      return cleanHTML(d.forms.messages.errors.title)(userInput)
   }
   else {
      return true
   }
}

/**
 * Validate generalized text, as Title (required)
 */
export const validateRequiredText = (errMsg: string) => (value: string): string | true => {
   // General text cannot be longer than 255 characters
   if (value.length > 255) {
      return errMsg
   }
   else if (value.length > 1) {
      return cleanHTML(errMsg)(value)
   }
   else {
      return errMsg
   }
}

 /**
 * Validate generalized text, as Subtitle (optional)
 */
export const validateOptionalText = (errMsg: string) => (value: string): string | true => {
   // General text cannot be longer than 255 characters
   if (value.length > 255) {
      return errMsg
   }
   else if (value.length > 1) {
      return cleanHTML(errMsg)(value)
   }
   else {
      return true
   }
}

/**
 * Validate DOIs
 */
export const _validateLegacyDOI = (userInput: string): boolean => {
   const post2008AllowedChars = [
      '-',
      '.',
      '_',
      ';',
      '(',
      ')',
      '/'
   ]
   const pre2008AllowedChars = [
      '#',
      '+',
      '<',
      '>'
   ]
   const allowedChars = [
      ...alphaNumeric,
      ...post2008AllowedChars,
      ...pre2008AllowedChars
   ]
   // DOIs must start with a 10.
   if (userInput.startsWith('10.')) {
      const cleanedInput = userInput.replace(/./g, (c) => {
         if (allowedChars.join().includes(c)) {
            return c
         }
         else {
            return ''
         }
      });
      if (cleanedInput === userInput) {
         return true
      }
      else {
         return false
      }
   }
   else {
      return false
   }
}

export const _validateDOI = (userInput: string): boolean => {
   const post2008AllowedChars = [
      '-',
      '.',
      '_',
      ';',
      '(',
      ')',
      '/'
   ]
   const allowedChars = [
      ...alphaNumeric,
      ...post2008AllowedChars
   ]
   // DOIs must start with a 10.
   if (userInput.startsWith('10.')) {
      const cleanedInput = userInput.replace(/./g, (c) => {
         if (allowedChars.join().includes(c)) {
            return c
         }
         else {
            return ''
         }
      });
      if (cleanedInput === userInput) {
         return true
      }
      else {
         return false
      }
   }
   else {
      return false
   }
}

export const validateOptPublisherDOI = (d: Translatables) => (userInput: string): string | true => {
   if (userInput.length > 1) {
      const isValid = _validateLegacyDOI(userInput);
      if (isValid) {
         return true
      }
      else {
         return d.forms.messages.errors.legacyDOI
      }
   }
   else {
      return true
   }
}
export const validateReqPublisherDOI = (d: Translatables) => (userInput: string): string | true => {
   if (userInput.length > 1) {
      const isValid = _validateLegacyDOI(userInput);
      if (isValid) {
         return true
      }
      else {
         return d.forms.messages.errors.legacyDOI
      }
   }
   else {
      return d.forms.messages.errors.legacyDOI
   }
}

/**
 * Vali-dates
 */
export const _stringToInt = (s: string): number | false => {
   // "Map" over each character
   const cleanedInput = s.replace(/./g, (c) => {
      if (numbers.join().includes(c)) {
         return c
      }
      else {
         return ''
      }
   });
   if (cleanedInput === s) {
      return Number(cleanedInput)
   }
   else {
      return false
   }
}
export const _validateYear = (userInput: string): boolean => {
   const int = _stringToInt(userInput);
   const today = new Date();
   if (userInput.length !== 4) {
      return false
   }
   else if (int >=1900 && int <= (today.getFullYear() + 2)) {
      return true
   }
   else {
      return false
   }
}
export const _validateMonth = (userInput: string): boolean => {
   const int = _stringToInt(userInput);
   if (userInput.length > 2 || userInput.length < 1) {
      return false
   }
   else if (int >=1 && int <= 12) {
      return true
   }
   else {
      return false
   }
}
export const _validateDay = (userInput: string): boolean => {
   const int = _stringToInt(userInput);
   if (userInput.length > 2 || userInput.length < 1) {
      return false
   }
   else if (int >=1 && int <= 31) {
      return true
   }
   else {
      return false
   }
}

export const validateYear = (errMsg: string) => (value: string): string | true => {
   if (value.length > 0) {
      const isValid = _validateYear(value);
      if (isValid) {
         return true
      }
      else {
         return errMsg
      }
   }
   else {
      return errMsg
   }
}
export const validateMonth = (errMsg: string) => (value: string): string | true => {
   if (value.length > 0) {
      const isValid = _validateMonth(value);
      if (isValid) {
         return true
      }
      else {
         return errMsg
      }
   }
   else {
      return true
   }
}
export const validateDay = (errMsg: string) => (value: string): string | true => {
   if (value.length > 0) {
      const isValid = _validateDay(value);
      if (isValid) {
         return true
      }
      else {
         return errMsg
      }
   }
   else {
      return true
   }
}

/**
 * Validate  Descriptions
 */
export const validateDescription = (d: FormText) => (userInput: string): string | true => {
   // Titles cannot be longer than 2,550 characters
   if (userInput.length > 2550) {
      return d.messages.errors.description
   }
   else if (userInput.length > 1) {
      return cleanHTML(d.messages.errors.description)(userInput)
   }
   else {
      return d.messages.errors.description
   }
}

/**
 * Try to ensure the copyright holder is not a common publisher
 */
export const validateNotAPublisher = (errMsg: string) => (userInput: string): string | true => {
   const publishers = [
      'elsevier',
      'wiley',
      'springer',
      'taylor and francis',
      'taylor & francis',
      'aaas'
   ];
   if (userInput.length > 255) {
      return errMsg
   }
   else if (userInput.length < 1) {
      // The copyright holder is required where the field is presented.
      return errMsg
   }
   else if (cleanHTML(errMsg)(userInput)) {
      // We have a valid string, so check for a publisher name
      let x = 0;
      for (let i = 0; i < publishers.length; i++) {
         // We'll increase the value of x, should the string include a publisher
         if (userInput.toLocaleLowerCase().includes(publishers[i])) {
            x++
         }
      }
      if (x === 0) {
         // If x is still 0, there were no publishers found
         return true
      }
      else {
         // If x is anything but 0, there was at least one publisher in the string
         return errMsg
      }
   }
   else {
      // The copyright holder is required where the field is presented.
      return errMsg
   }
}

/**
 * Validate license & copyright holder
 */
export const validateLicense = (d: FormText) => (value: string): string | true => {
   if (value === '') {
      // Licenses are required
      return d.messages.errors.license
   }
   else if (value.length > 1) {
      // The user selected a license
      return true
   }
   else {
      // Licenses are required
      return d.messages.errors.license
   }
}

/**
 * Validate Contributors
 */
export const validateContribs = (errMsg: string) => (value: Array<Contributor> | string): string | true => {
   if (typeof(value) === 'string') {
      // Contribs should only be a string if it's not set
      return errMsg
   }
   else if (value.length > 0) {
      // There is at least one contrib
      return true
   }
   else {
      // At least one contrib is required
      return errMsg
   }
}