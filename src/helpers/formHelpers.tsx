import * as React from 'react';
import Collapsible from 'react-collapsible';
import { NameStyle, ContribRole, ContribSequence, WorkRelationship, PeerReviewConducted, PeerReviewType, PeerReviewRecommendation, SubmissionFormText, SelectOption, } from '@octogroup/marxiv-common';
import { HastElement } from '@octogroup/hast-typescript';
const toJsx = require('@octogroup/hast-util-to-jsx');

/**
 * Determine Form Type
 */
export type FormType = 'preprint' | 'postprint' | 'oa' | 'uncopyrighted' | 'reportUnpublished' | 'thesis' | 'poster' | 'letter' | 'workingPaper';

export const determineSubmitFormType = () => {
   const path = window.location.pathname;
   // e.g. "/en/submit/preprint/"
   const workType = path.substr(11);
   if (workType.startsWith('preprint')) {
      return 'preprint'
   }
   else if (workType.startsWith('postprint')){
      return 'postprint'
   }
   else if (workType.startsWith('oa')){
      return 'oa'
   }
   else if (workType.startsWith('uncopyrighted')){
      return 'uncopyrighted'
   }
   else if (workType.startsWith('reportUnpublished')){
      return 'reportUnpublished'
   }
   else if (workType.startsWith('thesis')){
      return 'thesis'
   }
   else if (workType.startsWith('poster')){
      return 'poster'
   }
   else if (workType.startsWith('letter')){
      return 'letter'
   }
   else if (workType.startsWith('workingPaper')){
      return 'workingPaper'
   }
   else {
      return 'preprint'
   }
}

export const determineEditFormType = () => {
   const path = window.location.pathname;
   // e.g. "/en/edit/preprint/"
   const workType = path.substr(9);
   if (workType.startsWith('preprint')) {
      return 'preprint'
   }
   else if (workType.startsWith('postprint')){
      return 'postprint'
   }
   else if (workType.startsWith('oa')){
      return 'oa'
   }
   else if (workType.startsWith('uncopyrighted')){
      return 'uncopyrighted'
   }
   else if (workType.startsWith('reportUnpublished')){
      return 'reportUnpublished'
   }
   else if (workType.startsWith('thesis')){
      return 'thesis'
   }
   else if (workType.startsWith('poster')){
      return 'poster'
   }
   else if (workType.startsWith('letter')){
      return 'letter'
   }
   else if (workType.startsWith('workingPaper')){
      return 'workingPaper'
   }
   else {
      return 'preprint'
   }
}

export const determineModerateFormType = () => {
   const path = window.location.pathname;
   // e.g. "/en/moderate/preprint/"
   const workType = path.substr(13);
   if (workType.startsWith('preprint')) {
      return 'preprint'
   }
   else if (workType.startsWith('postprint')){
      return 'postprint'
   }
   else if (workType.startsWith('oa')){
      return 'oa'
   }
   else if (workType.startsWith('uncopyrighted')){
      return 'uncopyrighted'
   }
   else if (workType.startsWith('reportUnpublished')){
      return 'reportUnpublished'
   }
   else if (workType.startsWith('thesis')){
      return 'thesis'
   }
   else if (workType.startsWith('poster')){
      return 'poster'
   }
   else if (workType.startsWith('letter')){
      return 'letter'
   }
   else if (workType.startsWith('workingPaper')){
      return 'workingPaper'
   }
   else {
      return 'preprint'
   }
}

/**
 * Map values to labels for React-Select
 */
export class FormErrorMessage extends React.Component<{}, {}> {
   constructor(props: {}) {
      // Super
      super(props);
   }
   render() {
      return <div className='flex all-parent-row center formError strong'>
         {this.props.children}
      </div>
   }
}

/**
 * Convert Hast to React
 * 
 * Use the output like:
 * <div className='description' dangerouslySetInnerHTML={hastToReact(hastElement)} />
 */
export function hastToReact (element: HastElement): { __html: string } {
   const convertedJSX: string = toJsx(element);
   return {__html: convertedJSX}
}

/**
 * Make optional collapsible blocks
 */
export function makeOptionalCollapsible (d: SubmissionFormText, content: JSX.Element): JSX.Element {
   const title = <div className='collapsible-title'>
      {d.basic.optional.fieldTitle}
   </div>;
   return <div className='badge blue flex all-parent-row center'>
      <Collapsible trigger={title} classParentString='collapsible'>
         <div className='flex all-child-span100'>
            {content}
         </div>
      </Collapsible>
   </div>;
};

/**
 * Add selected prop tp SelectOption[]
 */
export const addSelected = (selected: string) => (opts: SelectOption[]): SelectOption[] => {
   let selectedOptions: SelectOption[] = new Array;
   for (let i = 0; i < opts.length; i++) {
      let opt = opts[i];
      if (opt.value === selected) {
         selectedOptions.push({
            ...opt,
            selected: true
         })
      }
      else {
         selectedOptions.push(opt)
      }
   }
   return selectedOptions
}

export const getDefaultValue = (value: string | undefined) => (opts: SelectOption[]): SelectOption => {
   let def: SelectOption = { value: '', label: '' };
   if (value) {
      for (let i = 0; i < opts.length; i++) {
         if (opts[i].value === value) {
            def = opts[i];
         }
      }
   }
   return def
}

export const getDefaultMultipleValues = (values: string[] | undefined) => (opts: SelectOption[]): SelectOption[] => {
   if (values && (values.length > 0)) {
      let selectedOpts: SelectOption[] = new Array;
      values.map((value: string) => {
         for (let i = 0; i < opts.length; i++) {
            if (opts[i].value === value) {
               selectedOpts.push(opts[i]);
            }
         }
      });
      return selectedOpts
   }
   else {
      return []
   }
}

export const getDefaultOrder = (value: number) => (opts: SelectOption[]): SelectOption => {
   let def: SelectOption = { value: '', label: '' };
   for (let i = 0; i < opts.length; i++) {
      if (opts[i].value === value.toString()) {
         def = opts[i];
      }
   }
   return def
}

/**
 * Common interfaces
 * 
 * @param order Number as a string
 */
export interface Contributor {
   surname: string
   nameStyle: NameStyle
   role: ContribRole
   sequence: ContribSequence
   givenName?: string
   email?: string
   affiliation?: string
   orcid?: string
   suffix?: string
   // Form helpers
   id: string
   order: string
   isOrg: 'on' | undefined;
}

export interface Institution {
   name: string
   acronym?: string
   location?: string
   department?: string
}

export interface Relationship {
   doi: string
   type: WorkRelationship | ''
   // Form helpers
   id: string
}

export interface Citation {
   doi?: string
   isbn?: string
   issn?: string
   articleTitle?: string
   journalTitle?: string
   surname?: string
   volume?: string
   issue?: string
   page?: string
   year?: string
   edition?: string
   component?: string
   seriesTitle?: string
   volumeTitle?: string
   unstructured?: string
}

export interface ConferenceEventDates {
   startYear: string
   startMonth: string
   startDay: string
   endYear: string
   endMonth: string
   endDay: string
}

export interface ConferenceEvent {
   name: string
   dates: ConferenceEventDates
   theme?: string
   acronym?: string
   number?: string
   sponsor?: string
   location?: string
}

export interface ConferencePaper {
   conferenceEvent: ConferenceEvent
   fullText: boolean | string
   firstPage?: string
   lastPage?: string
}

export interface CrossrefDate {
   year: string
   month?: string
   day?: string
}

export interface Database {
   title: string
   description: string
   datePublished: CrossrefDate
   dateCreated?: CrossrefDate
   dateUpdated?: CrossrefDate
}

export interface Publisher {
   name: string
   location?: string
}

export interface PeerReview {
   doiofReviewedWork: string
   conducted: PeerReviewConducted | ''
   type: PeerReviewType | ''
   recommendation: PeerReviewRecommendation | ''
   competingInterestStatement?: string
}

/**
 * Create IDs for Contribs and Relationships
 */
export const createID = (): string => (Math.random() * 1000000000).toString();