import { useMutation, useQuery, ApolloProvider } from '@apollo/react-hooks';
import * as React from 'react'
import { setContext } from 'apollo-link-context'
import { createHttpLink } from "apollo-link-http";
import { Either, Maybe } from "@octogroup/fairy-land"
import { ApolloClient, InMemoryCache } from "apollo-boost"
interface UserRegistrationMutation {
   username: String
   email: string
   emailConfirm: string
   password: string
   passwordConfirm: string
   given?: string
   surname?: string
   org?: string
   job?: string
   orcid?: string
   country?: string
   timezone?: string
   newsletter?: string
}

interface UserRegistrationForm extends UserRegistrationMutation {
   emailConfirm: string
   passwordConfirm: string
}


interface OctoLocalStorage {
   submissions?: {
      [timestamp: string]: {
         completed: boolean
         formState: any
      }
   }
}

interface OctoSessionStorage {
   authToken?: string
   userRegistrationForm?: UserRegistrationForm
}

export interface OctoStore {
   endpoint: string
   session: OctoSessionStorage
   local: OctoLocalStorage
}

export const withApolloClient = (uri: string) => {
   let link = createHttpLink({
      uri,
      credentials: 'include'
   });
   let cache = new InMemoryCache()
   let client = new ApolloClient({
      cache,
      link,
   });
   return {
      client,
      withAuth: (token: string) => {
         link = createHttpLink({
            ...link,
            headers: {
               Authorization: token
            }
         })
         client = new ApolloClient({
            cache,
            link
         })
      }
   }
}

export function useOcto(init: OctoStore, uri: string) {
   const fullKey = '__marxiv__'
   const item: string | null = localStorage.getItem(fullKey)
   const sessionItem: string | null = sessionStorage.getItem(fullKey)
   const initStore: OctoStore =
      item ? ({ ...init, local: JSON.parse(item), session: JSON.parse(item) }) : init
   const [state, modify] = React.useState(initStore)
   const { withAuth, client } = withApolloClient(uri)
   const setState = (next: OctoStore): void => React.useEffect(() => {
      modify(next);
      localStorage.setItem(fullKey, JSON.stringify(next.local));
      sessionStorage.setItem(fullKey, JSON.stringify(next.session));
   })
   return {
      state,
      setState,
      withAuth,
      client
   }
}
