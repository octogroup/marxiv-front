import * as React from 'react';
import { render } from "react-dom";
import { Translatables, translatablesEnglish, LicenseOption, englishMarXivMinimalSitemap } from '@octogroup/marxiv-common';
import { MarXivSitemapMinimum } from '@octogroup/marxiv-common/dist/sitemap';
import useForm from 'react-hook-form'
import gql from 'graphql-tag';


import { HookContribs } from '../submitForms/contributors';
import { HookFileUpload, FileUploadState } from '../submitForms/upload';
import { HookBasicInfo, PostedContentState } from '../submitForms/basicInfo';
import { HookRelationships } from '../submitForms/relationships';
import { HookLicenseCopyright, LicenseState } from '../submitForms/license';
import { reCAPTCHAv3SiteKey, InvokeReCAPTCHAOptions, rcptaBackoff, } from '../scripts/recaptchaSetup';
import { Contributor, hastToReact, Relationship, determineEditFormType, FormType } from '../helpers/formHelpers';
import { addEventListeners } from '../scripts/eventListeners';

/**
 * We can extract the Work ID with `window.location.search` 
 * 
 * if `window.location.href` = "http://marxiv.test/en/moderate/preprint/?id=1"
 * then
 * `window.location.search` = "?id=1"
 */

/**
 * Ensure that license and peerReviewed cannot be empty strings
 */
export interface EditFormState extends FileUploadState, PostedContentState, LicenseState {
   license: LicenseOption
   peerReviewed: 'peerReviewed' | 'notPeerReviewed'
   // Contribs
   contributors: Contributor[]
   // Relationships between works
   relationships: Relationship[]
   // Was this work rejected?
   resubmission: true | undefined
   moderationMessage: string | undefined
}

const graphqlGetWorkData = gql`
   query getWorkData($paperId: number!) {
      submitPreprint(id: $paperId) {
         workInfo
      }
   }`;

const HookEditPreprint = function ({ getExistingData, submitBackend, d, siteMap }: { getExistingData: () => EditFormState, submitBackend: (u: EditFormState) => void, d: Translatables, siteMap: MarXivSitemapMinimum }) {
   // Populate default values from existing data.
   const initialFormState = getExistingData();

   const { register, unregister, getValues, setValue, errors, watch, handleSubmit } = useForm<EditFormState>({
      mode: 'onBlur',
      reValidateMode: 'onBlur',
      defaultValues: initialFormState
   });

   React.useEffect(() => {
      // Add event listeners just on load
      return addEventListeners();
   }, []);

   /**
    * Redirect the user to the confirmation page.
    */
   const confirmationRedirect = (): string => {
      const path: string = window.location.pathname; // "/en/submit/preprint/"
      const lang: string = path.slice(0, 3); // "/en"
      if (initialFormState.resubmission) {
         const confirmationPath: string = lang + siteMap.confirm.resubmit.localURL;
         return window.location.pathname = confirmationPath;
      }
      else {
         const confirmationPath: string = lang + siteMap.confirm.updatedWork.localURL;
         return window.location.pathname = confirmationPath;
      }
   }

   const onSubmitCB = data => {
      console.log("Submitted parent form with data:");
      console.log(data);

      // Redirect to confirmation page
      confirmationRedirect();
   }

   /**
    * Determine the form type
    */
   const formType: FormType = determineEditFormType();

   const isPostprint: boolean = formType === 'postprint' ? true : false;

   const getFormTitle = (formType: FormType): string => {
      switch (formType) {
         case 'preprint':
            return d.forms.edit.titles.preprint;
         case 'postprint':
            return d.forms.edit.titles.postprint;
         case 'oa':
            return d.forms.edit.titles.openAccess;
         case 'uncopyrighted':
            return d.forms.edit.titles.unCopyrighted;
         case 'reportUnpublished':
            return d.forms.edit.titles.unpublishedReport;
         case 'thesis':
            return d.forms.edit.titles.thesis;
         case 'poster':
            return d.forms.edit.titles.posterPresentationSI;
         case 'letter':
            return d.forms.edit.titles.letter;
         case 'workingPaper':
            return d.forms.edit.titles.workingPaper;
         default:
            return d.forms.edit.titles.preprint;
      }
   }

   /**
    * We need to wait until after the reCAPTCHA script has loaded.
    * Once it has, `grecaptcha` will be defined
    */
   const onLoadForm: InvokeReCAPTCHAOptions = {
      sitekey: reCAPTCHAv3SiteKey,
      action: 'form'
   };

   return <div id='form' className='form'>
      <h1>
         {getFormTitle(formType)}
      </h1>

      {initialFormState.resubmission ? (<div dangerouslySetInnerHTML={hastToReact(d.forms.edit.rejectedNote)} />) : (<div dangerouslySetInnerHTML={hastToReact(d.forms.edit.editNote)} />)}
      
      {initialFormState.moderationMessage ? (<p className='strong'>{initialFormState.moderationMessage}</p>) : null}

      <div className='verticalBuffer1'>
            <form onSubmit={handleSubmit(onSubmitCB)} id='uploadPreprint'>

               <div className='verticalBuffer1'>
                  <HookFileUpload
                     d={translatablesEnglish.translatables}
                     isEditForm={true}
                     register={register}
                     setValue={setValue}
                     errors={errors}
                     watch={watch}
                  />
               </div>

               <div className='verticalBuffer1'>
                  <HookBasicInfo
                     d={translatablesEnglish.translatables}
                     initialValues={{
                        title: initialFormState.title,
                        language: initialFormState.language,
                        peerReviewed: initialFormState.peerReviewed,
                        subtitle: initialFormState.subtitle,
                        publisherDOI: initialFormState.publisherDOI,
                        description: initialFormState.description,
                        originalPublicationYear: initialFormState.originalPublicationYear,
                        originalPublicationMonth: initialFormState.originalPublicationMonth,
                        originalPublicationDay: initialFormState.originalPublicationDay,
                     }}
                     isPostprint={isPostprint}
                     register={register}
                     setValue={setValue}
                     errors={errors}
                  />
               </div>

               <div className='verticalBuffer1'>
                  <HookContribs
                     d={translatablesEnglish.translatables}
                     register={register}
                     setValue={setValue}
                     getValues={getValues}
                  />
               </div>

               <div className='verticalBuffer1'>
                  <HookRelationships
                     d={translatablesEnglish.translatables}
                     register={register}
                     setValue={setValue}
                     getValues={getValues}
                  />
               </div>

               <div className='verticalBuffer1'>
                  <HookLicenseCopyright
                     d={translatablesEnglish.translatables}
                     initialValues={{
                        license: initialFormState.license,
                        copyrightHolder: initialFormState.copyrightHolder,
                     }}
                     isPostprint={isPostprint}
                     register={register}
                     unregister={unregister}
                     watch={watch}
                     errors={errors}
                     setValue={setValue}
                  />
               </div>

               <div className='form end flex all-parent-row'>
                  <div className='flex all-child-span1 center'>
                     <button type='reset' className='button cancel'>
                        {d.commonReplacements.cancel}
                     </button>
                  </div>
                  <div className='flex all-child-span1 center'>
                     <button type='submit' className='button submit' onClick={handleSubmit(onSubmitCB)}>
                        {d.commonReplacements.submit}
                     </button>
                  </div>
               </div>

            </form>
      </div>

      {() => rcptaBackoff(onLoadForm)}

   </div>
}

/**
 * Dummy backend to get data
 */
const getExistingData = (): EditFormState => ({
   // File upload
   file: '',
   fileData: '',
   // Basic information
   title: 'This is the title of a work',
   language: 'en',
   peerReviewed: 'notPeerReviewed',
   subtitle: 'This is a subtitle',
   publisherDOI: '10.321/osf.io/testDOI',
   // Original publication date
   originalPublicationYear: '2019',
   originalPublicationMonth: '10',
   originalPublicationDay: '31',
   // License & copyright
   license: 'ccBy',
   copyrightHolder: '',
   // Description
   description: 'Hello, kitty!',
   // Contribututors
   contributors: [{
      surname: 'OCTO',
      nameStyle: 'western',
      role: 'author',
      sequence: 'first',
      order: '1',
      isOrg: 'on',
      id: '125874619844.268434164'
   }],
   // Relationships between works
   relationships: [],
   // Rejected work testing
   resubmission: true,
   moderationMessage: 'This work was rejected because mercats do not exist.'
})

/**
 * Mount to the DOM
 */
const englishEditPreprintForm =
   <HookEditPreprint
      getExistingData={getExistingData}
      submitBackend={(data: any) => console.log(data)}
      d={translatablesEnglish.translatables}
      siteMap={englishMarXivMinimalSitemap}
   />;

const domContainer = document.getElementById('reactEditForm');

if (domContainer) {
   render(englishEditPreprintForm, domContainer);
}